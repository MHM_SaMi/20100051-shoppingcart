/*
 * Copyright (C) 2014 White_Birds Open Source Project 
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at 
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software 
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and 
 * limitations under the License.
 */

package PersistenceLayer;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import org.junit.Test;
import org.junit.runner.JUnitCore;
import org.junit.runner.Result;
import org.junit.runner.notification.Failure;

import Exception.FileException;
import ShoppingCart.IShoppingCart;

/**
 * FilePersistenceTest is a unit test class for class FilePersistence
 * 
 * @see PersistenceLayer.FilePersistence
 * @author White_Birds
 */
public class FilePersistenceTest {

	/**
	 * Test for method: createCart(int,int)
	 * 
	 * @throws Throwable
	 *             Tests may throw any Throwable
	 * @see FilePersistence#createCart(int,int)
	 * @author White_Birds
	 */
	@Test(expected = java.io.FileNotFoundException.class)
	public void testCreateCart1() throws Throwable {
		FilePersistence testedObject = (FilePersistence) FilePersistence
				.getInstance();
		@SuppressWarnings("unused")
		IShoppingCart result = testedObject.createCart(-1000, 256);
		// java.io.FileNotFoundException thrown
	}

	/**
	 * Test for method: createCart(int,int)
	 * 
	 * @throws Throwable
	 *             Tests may throw any Throwable
	 * @see FilePersistence#createCart(int,int)
	 * @author White_Birds
	 */
	@Test
	public void testCreateCart2() throws Throwable {
		FilePersistence testedObject = (FilePersistence) FilePersistence
				.getInstance();
		IShoppingCart result = testedObject.createCart(-10, 100);
		assertNotNull(result);
		assertEquals(100, result.getCustomerID());
		assertEquals(-10, result.getSessionID());
		assertNotNull(result.getLastAccessedDate());
		assertEquals(new java.util.Date().toString(), result
				.getLastAccessedDate().toString());
		assertNotNull(result.getItems());
		assertEquals(0, result.getItems().size());
		// No exception thrown
	}

	/**
	 * Test for method: createCart(int,int)
	 * 
	 * @throws Throwable
	 *             Tests may throw any Throwable
	 * @see FilePersistence#createCart(int,int)
	 * @author White_Birds
	 */
	@Test(expected = java.io.FileNotFoundException.class)
	public void testCreateCart3() throws Throwable {
		FilePersistence testedObject = (FilePersistence) FilePersistence
				.getInstance();
		@SuppressWarnings("unused")
		IShoppingCart result = testedObject.createCart(920, 5);
		// java.io.FileNotFoundException thrown
	}

	/**
	 * Test for method: createCart(int,int)
	 * 
	 * @throws Throwable
	 *             Tests may throw any Throwable
	 * @see FilePersistence#createCart(int,int)
	 * @author White_Birds
	 */
	@Test(expected = java.lang.NullPointerException.class)
	public void testCreateCart4() throws Throwable {
		FilePersistence testedObject = (FilePersistence) FilePersistence
				.getInstance();
		@SuppressWarnings("unused")
		IShoppingCart result = testedObject.createCart(-2147483648, -1000);
		// NullPointerException thrown
	}

	/**
	 * Test for method: createCart(int,int)
	 * 
	 * @throws Throwable
	 *             Tests may throw any Throwable
	 * @see FilePersistence#createCart(int,int)
	 * @author White_Birds
	 */
	@Test(expected = ClassCastException.class)
	public void testCreateCart5() throws Throwable {
		FilePersistence testedObject = (FilePersistence) FilePersistence
				.getInstance();
		@SuppressWarnings("unused")
		IShoppingCart result = testedObject.createCart(858, 2147483647);
		// ClassCastException: java.lang.String cannot be cast to
		// ShoppingCart.IShoppingCart thrown
	}

	/**
	 * Test for method: createCart(int,int)
	 * 
	 * @throws Throwable
	 *             Tests may throw any Throwable
	 * @see FilePersistence#createCart(int,int)
	 * @author White_Birds
	 */
	@Test(expected = java.lang.NullPointerException.class)
	public void testCreateCart6() throws Throwable {
		FilePersistence testedObject = (FilePersistence) FilePersistence
				.getInstance();
		@SuppressWarnings("unused")
		IShoppingCart result = testedObject.createCart(100, 1000);
		// NullPointerException thrown
	}

	/**
	 * Test for method: createCart(int,int)
	 * 
	 * @throws Throwable
	 *             Tests may throw any Throwable
	 * @see FilePersistence#createCart(int,int)
	 * @author White_Birds
	 */
	@Test(expected = java.io.IOException.class)
	public void testCreateCart7() throws Throwable {
		FilePersistence testedObject = (FilePersistence) FilePersistence
				.getInstance();
		@SuppressWarnings("unused")
		IShoppingCart result = testedObject.createCart(5, -10);
		// java.io.IOException thrown
	}

	/**
	 * /** Test for method: createCart(int,int)
	 * 
	 * @throws Throwable
	 *             Tests may throw any Throwable
	 * @see FilePersistence#createCart(int,int)
	 * @author White_Birds
	 */
	@Test(expected = ClassCastException.class)
	public void testCreateCart8() throws Throwable {
		FilePersistence testedObject = (FilePersistence) FilePersistence
				.getInstance();
		@SuppressWarnings("unused")
		IShoppingCart result = testedObject.createCart(256, 920);
		// ClassCastException: java.lang.String cannot be cast to
		// java.util.ArrayList thrown
	}

	/**
	 * Test for method: createCart(int,int)
	 * 
	 * @throws Throwable
	 *             Tests may throw any Throwable
	 * @see FilePersistence#createCart(int,int)
	 * @author White_Birds
	 */
	@Test(expected = java.io.IOException.class)
	public void testCreateCart9() throws Throwable {
		FilePersistence testedObject = (FilePersistence) FilePersistence
				.getInstance();
		@SuppressWarnings("unused")
		IShoppingCart result = testedObject.createCart(-2147483648, -1000);
		// java.io.IOException thrown
	}

	/**
	 * Test for method: createCart(int,int)
	 * 
	 * @throws Throwable
	 *             Tests may throw any Throwable
	 * @see FilePersistence#createCart(int,int)
	 * @author White_Birds
	 */
	@Test(expected = ClassNotFoundException.class)
	public void testCreateCart10() throws Throwable {
		FilePersistence testedObject = (FilePersistence) FilePersistence
				.getInstance();
		@SuppressWarnings("unused")
		IShoppingCart result = testedObject.createCart(858, 2147483647);
		// ClassNotFoundException thrown
	}

	/**
	 * Test for method: createCart(int,int)
	 * 
	 * @throws Throwable
	 *             Tests may throw any Throwable
	 * @see FilePersistence#createCart(int,int)
	 * @author White_Birds
	 */
	@Test(expected = java.io.StreamCorruptedException.class)
	public void testCreateCart11() throws Throwable {
		FilePersistence testedObject = (FilePersistence) FilePersistence
				.getInstance();
		@SuppressWarnings("unused")
		IShoppingCart result = testedObject.createCart(256, 920);
		// java.io.StreamCorruptedException thrown
	}

	/**
	 * Test for method: createCart(int,int)
	 * 
	 * @throws Throwable
	 *             Tests may throw any Throwable
	 * @see FilePersistence#createCart(int,int)
	 * @author White_Birds
	 */
	@Test(expected = java.io.IOException.class)
	public void testCreateCart12() throws Throwable {
		FilePersistence testedObject = (FilePersistence) FilePersistence
				.getInstance();
		@SuppressWarnings("unused")
		IShoppingCart result = testedObject.createCart(858, 2147483647);
		// java.io.IOException thrown
	}

	/**
	 * Test for method: createCart(int,int)
	 * 
	 * @throws Throwable
	 *             Tests may throw any Throwable
	 * @see FilePersistence#createCart(int,int)
	 * @author White_Birds
	 */
	@Test
	public void testCreateCart13() throws Throwable {
		FilePersistence testedObject = (FilePersistence) FilePersistence
				.getInstance();
		IShoppingCart result = testedObject.createCart(100, 1000);
		assertNotNull(result);
		assertEquals(1000, result.getCustomerID());
		assertEquals(100, result.getSessionID());
		assertNotNull(result.getLastAccessedDate());
		assertEquals(new java.util.Date().toString(), result
				.getLastAccessedDate().toString());
		assertNotNull(result.getItems());
		assertEquals(0, result.getItems().size());
		// No exception thrown
	}

	/**
	 * Test for method: createCart(int,int)
	 * 
	 * @throws Throwable
	 *             Tests may throw any Throwable
	 * @see FilePersistence#createCart(int,int)
	 * @author White_Birds
	 */
	@Test(expected = java.io.IOException.class)
	public void testCreateCart14() throws Throwable {
		FilePersistence testedObject = (FilePersistence) FilePersistence
				.getInstance();
		@SuppressWarnings("unused")
		IShoppingCart result = testedObject.createCart(5, -10);
		// java.io.IOException thrown
	}

	/**
	 * Test for method: createCart(int,int)
	 * 
	 * @throws Throwable
	 *             Tests may throw any Throwable
	 * @see FilePersistence#createCart(int,int)
	 * @author White_Birds
	 */
	@Test(expected = java.io.IOException.class)
	public void testCreateCart15() throws Throwable {
		FilePersistence testedObject = (FilePersistence) FilePersistence
				.getInstance();
		@SuppressWarnings("unused")
		IShoppingCart result = testedObject.createCart(256, 920);
		// java.io.IOException thrown
	}

	/**
	 * Test for method: createCart(int,int)
	 * 
	 * @throws Throwable
	 *             Tests may throw any Throwable
	 * @see FilePersistence#createCart(int,int)
	 * @author White_Birds
	 */
	@Test(expected = java.io.FileNotFoundException.class)
	public void testCreateCart16() throws Throwable {
		FilePersistence testedObject = (FilePersistence) FilePersistence
				.getInstance();
		@SuppressWarnings("unused")
		IShoppingCart result = testedObject.createCart(858, 2147483647);
		// java.io.FileNotFoundException thrown
	}

	/**
	 * Test for method: createCart(int,int)
	 * 
	 * @throws Throwable
	 *             Tests may throw any Throwable
	 * @see FilePersistence#createCart(int,int)
	 * @author White_Birds
	 */
	@Test(expected = java.io.IOException.class)
	public void testCreateCart17() throws Throwable {
		FilePersistence testedObject = (FilePersistence) FilePersistence
				.getInstance();
		@SuppressWarnings("unused")
		IShoppingCart result = testedObject.createCart(256, 920);
		// java.io.IOException thrown
	}

	/**
	 * Test for method: createCart(int,int)
	 * 
	 * @throws Throwable
	 *             Tests may throw any Throwable
	 * @see FilePersistence#createCart(int,int)
	 * @author White_Birds
	 */
	@Test(expected = java.io.IOException.class)
	public void testCreateCart18() throws Throwable {
		FilePersistence testedObject = (FilePersistence) FilePersistence
				.getInstance();
		@SuppressWarnings("unused")
		IShoppingCart result = testedObject.createCart(858, 2147483647);
		// java.io.IOException thrown
	}

	/**
	 * Test for method: getInstance()
	 * 
	 * @throws Throwable
	 *             Tests may throw any Throwable
	 * @see FilePersistence#getInstance()
	 * @author White_Birds
	 */
	@Test
	public void testGetInstance() throws Throwable {
		IPersistenceMechanism result = FilePersistence.getInstance();
		assertNotNull(result);
		// No exception thrown
	}

	/**
	 * Test for method: loadCart(int)
	 * 
	 * @throws Throwable
	 *             Tests may throw any Throwable
	 * @see FilePersistence#loadCart(int)
	 * @author White_Birds
	 */
	@Test(expected = java.io.FileNotFoundException.class)
	public void testLoadCart1() throws Throwable {
		FilePersistence testedObject = (FilePersistence) FilePersistence
				.getInstance();
		@SuppressWarnings("unused")
		IShoppingCart result = testedObject.loadCart(-2147483648);
		// java.io.FileNotFoundException thrown
	}

	/**
	 * Test for method: loadCart(int)
	 * 
	 * @throws Throwable
	 *             Tests may throw any Throwable
	 * @see FilePersistence#loadCart(int)
	 * @author White_Birds
	 */
	@Test(expected = FileException.class)
	public void testLoadCart2() throws Throwable {
		FilePersistence testedObject = (FilePersistence) FilePersistence
				.getInstance();
		@SuppressWarnings("unused")
		IShoppingCart result = testedObject.loadCart(920);
		// Exception.FileException thrown
	}

	/**
	 * Test for method: loadCart(int)
	 * 
	 * @throws Throwable
	 *             Tests may throw any Throwable
	 * @see FilePersistence#loadCart(int)
	 * @author White_Birds
	 */
	@Test(expected = java.lang.NullPointerException.class)
	public void testLoadCart3() throws Throwable {
		FilePersistence testedObject = (FilePersistence) FilePersistence
				.getInstance();
		@SuppressWarnings("unused")
		IShoppingCart result = testedObject.loadCart(-2147483648);
		// NullPointerException thrown
	}

	/**
	 * Test for method: loadCart(int)
	 * 
	 * @throws Throwable
	 *             Tests may throw any Throwable
	 * @see FilePersistence#loadCart(int)
	 * @author White_Birds
	 */
	@Test(expected = ClassCastException.class)
	public void testLoadCart4() throws Throwable {
		FilePersistence testedObject = (FilePersistence) FilePersistence
				.getInstance();
		@SuppressWarnings("unused")
		IShoppingCart result = testedObject.loadCart(2147483647);
		// ClassCastException: java.lang.String cannot be cast to
		// ShoppingCart.IShoppingCart thrown
	}

	/**
	 * Test for method: loadCart(int)
	 * 
	 * @throws Throwable
	 *             Tests may throw any Throwable
	 * @see FilePersistence#loadCart(int)
	 * @author White_Birds
	 */
	@Test(expected = java.lang.NullPointerException.class)
	public void testLoadCart5() throws Throwable {
		FilePersistence testedObject = (FilePersistence) FilePersistence
				.getInstance();
		@SuppressWarnings("unused")
		IShoppingCart result = testedObject.loadCart(1000);
		// NullPointerException thrown
	}

	/**
	 * Test for method: loadCart(int)
	 * 
	 * @throws Throwable
	 *             Tests may throw any Throwable
	 * @see FilePersistence#loadCart(int)
	 * @author White_Birds
	 */
	@Test(expected = java.io.IOException.class)
	public void testLoadCart6() throws Throwable {
		FilePersistence testedObject = (FilePersistence) FilePersistence
				.getInstance();
		@SuppressWarnings("unused")
		IShoppingCart result = testedObject.loadCart(100);
		// java.io.IOException thrown
	}

	/**
	 * Test for method: loadCart(int)
	 * 
	 * @throws Throwable
	 *             Tests may throw any Throwable
	 * @see FilePersistence#loadCart(int)
	 * @author White_Birds
	 */
	@Test(expected = ClassCastException.class)
	public void testLoadCart7() throws Throwable {
		FilePersistence testedObject = (FilePersistence) FilePersistence
				.getInstance();
		@SuppressWarnings("unused")
		IShoppingCart result = testedObject.loadCart(-10);
		// ClassCastException: java.lang.String cannot be cast to
		// java.util.ArrayList thrown
	}

	/**
	 * Test for method: loadCart(int)
	 * 
	 * @throws Throwable
	 *             Tests may throw any Throwable
	 * @see FilePersistence#loadCart(int)
	 * @author White_Birds
	 */
	@Test(expected = java.io.IOException.class)
	public void testLoadCart8() throws Throwable {
		FilePersistence testedObject = (FilePersistence) FilePersistence
				.getInstance();
		@SuppressWarnings("unused")
		IShoppingCart result = testedObject.loadCart(5);
		// java.io.IOException thrown
	}

	/**
	 * Test for method: loadCart(int)
	 * 
	 * @throws Throwable
	 *             Tests may throw any Throwable
	 * @see FilePersistence#loadCart(int)
	 * @author White_Birds
	 */
	@Test(expected = ClassNotFoundException.class)
	public void testLoadCart9() throws Throwable {
		FilePersistence testedObject = (FilePersistence) FilePersistence
				.getInstance();
		@SuppressWarnings("unused")
		IShoppingCart result = testedObject.loadCart(920);
		// ClassNotFoundException thrown
	}

	/**
	 * Test for method: loadCart(int)
	 * 
	 * @throws Throwable
	 *             Tests may throw any Throwable
	 * @see FilePersistence#loadCart(int)
	 * @author White_Birds
	 */
	@Test(expected = java.io.FileNotFoundException.class)
	public void testLoadCart10() throws Throwable {
		FilePersistence testedObject = (FilePersistence) FilePersistence
				.getInstance();
		@SuppressWarnings("unused")
		IShoppingCart result = testedObject.loadCart(-1000);
		// java.io.FileNotFoundException thrown
	}

	/**
	 * Test for method: loadCart(int)
	 * 
	 * @throws Throwable
	 *             Tests may throw any Throwable
	 * @see FilePersistence#loadCart(int)
	 * @author White_Birds
	 */
	@Test(expected = java.io.StreamCorruptedException.class)
	public void testLoadCart11() throws Throwable {
		FilePersistence testedObject = (FilePersistence) FilePersistence
				.getInstance();
		@SuppressWarnings("unused")
		IShoppingCart result = testedObject.loadCart(858);
		// java.io.StreamCorruptedException thrown
	}

	/**
	 * Test for method: loadCart(int)
	 * 
	 * @throws Throwable
	 *             Tests may throw any Throwable
	 * @see FilePersistence#loadCart(int)
	 * @author White_Birds
	 */
	@Test(expected = java.io.IOException.class)
	public void testLoadCart12() throws Throwable {
		FilePersistence testedObject = (FilePersistence) FilePersistence
				.getInstance();
		@SuppressWarnings("unused")
		IShoppingCart result = testedObject.loadCart(-10);
		// java.io.IOException thrown
	}

	/**
	 * Test for method: loadCart(int)
	 * 
	 * @throws Throwable
	 *             Tests may throw any Throwable
	 * @see FilePersistence#loadCart(int)
	 * @author White_Birds
	 */
	@Test(expected = java.io.IOException.class)
	public void testLoadCart13() throws Throwable {
		FilePersistence testedObject = (FilePersistence) FilePersistence
				.getInstance();
		@SuppressWarnings("unused")
		IShoppingCart result = testedObject.loadCart(858);
		// java.io.IOException thrown
	}

	/**
	 * Test for method: loadCart(int)
	 * 
	 * @throws Throwable
	 *             Tests may throw any Throwable
	 * @see FilePersistence#loadCart(int)
	 * @author White_Birds
	 */
	@Test(expected = java.io.IOException.class)
	public void testLoadCart14() throws Throwable {
		FilePersistence testedObject = (FilePersistence) FilePersistence
				.getInstance();
		@SuppressWarnings("unused")
		IShoppingCart result = testedObject.loadCart(100);
		// java.io.IOException thrown
	}

	/**
	 * Test for method: loadCart(int,int)
	 * 
	 * @throws Throwable
	 *             Tests may throw any Throwable
	 * @see FilePersistence#loadCart(int,int)
	 * @author White_Birds
	 */
	@Test(expected = java.io.FileNotFoundException.class)
	public void testLoadCart15() throws Throwable {
		FilePersistence testedObject = (FilePersistence) FilePersistence
				.getInstance();
		@SuppressWarnings("unused")
		IShoppingCart result = testedObject.loadCart(-2147483648, -1000);
		// java.io.FileNotFoundException thrown
	}

	/**
	 * Test for method: loadCart(int,int)
	 * 
	 * @throws Throwable
	 *             Tests may throw any Throwable
	 * @see FilePersistence#loadCart(int,int)
	 * @author White_Birds
	 */
	@Test(expected = NullPointerException.class)
	public void testLoadCart16() throws Throwable {
		FilePersistence testedObject = (FilePersistence) FilePersistence
				.getInstance();
		@SuppressWarnings("unused")
		IShoppingCart result = testedObject.loadCart(100, 1000);
		// NullPointerException thrown
	}

	/**
	 * Test for method: loadCart(int,int)
	 * 
	 * @throws Throwable
	 *             Tests may throw any Throwable
	 * @see FilePersistence#loadCart(int,int)
	 * @author White_Birds
	 */
	@Test(expected = ClassCastException.class)
	public void testLoadCart17() throws Throwable {
		FilePersistence testedObject = (FilePersistence) FilePersistence
				.getInstance();
		@SuppressWarnings("unused")
		IShoppingCart result = testedObject.loadCart(5, -10);
		// ClassCastException: java.lang.String cannot be cast to
		// ShoppingCart.IShoppingCart thrown
	}

	/**
	 * Test for method: loadCart(int,int)
	 * 
	 * @throws Throwable
	 *             Tests may throw any Throwable
	 * @see FilePersistence#loadCart(int,int)
	 * @author White_Birds
	 */
	@Test(expected = NullPointerException.class)
	public void testLoadCart18() throws Throwable {
		FilePersistence testedObject = (FilePersistence) FilePersistence
				.getInstance();
		@SuppressWarnings("unused")
		IShoppingCart result = testedObject.loadCart(-2147483648, -1000);
		// NullPointerException thrown
	}

	/**
	 * Test for method: loadCart(int,int)
	 * 
	 * @throws Throwable
	 *             Tests may throw any Throwable
	 * @see FilePersistence#loadCart(int,int)
	 * @author White_Birds
	 */
	@Test(expected = java.io.IOException.class)
	public void testLoadCart19() throws Throwable {
		FilePersistence testedObject = (FilePersistence) FilePersistence
				.getInstance();
		@SuppressWarnings("unused")
		IShoppingCart result = testedObject.loadCart(858, 2147483647);
		// java.io.IOException thrown
	}

	/**
	 * Test for method: loadCart(int,int)
	 * 
	 * @throws Throwable
	 *             Tests may throw any Throwable
	 * @see FilePersistence#loadCart(int,int)
	 * @author White_Birds
	 */
	@Test(expected = ClassCastException.class)
	public void testLoadCart20() throws Throwable {
		FilePersistence testedObject = (FilePersistence) FilePersistence
				.getInstance();
		@SuppressWarnings("unused")
		IShoppingCart result = testedObject.loadCart(100, 1000);
		// ClassCastException: java.lang.String cannot be cast to
		// java.util.ArrayList thrown
	}

	/**
	 * Test for method: loadCart(int,int)
	 * 
	 * @throws Throwable
	 *             Tests may throw any Throwable
	 * @see FilePersistence#loadCart(int,int)
	 * @author White_Birds
	 */
	@Test(expected = java.io.IOException.class)
	public void testLoadCart21() throws Throwable {
		FilePersistence testedObject = (FilePersistence) FilePersistence
				.getInstance();
		@SuppressWarnings("unused")
		IShoppingCart result = testedObject.loadCart(5, -10);
		// java.io.IOException thrown
	}

	/**
	 * Test for method: loadCart(int,int)
	 * 
	 * @throws Throwable
	 *             Tests may throw any Throwable
	 * @see FilePersistence#loadCart(int,int)
	 * @author White_Birds
	 */
	@Test(expected = ClassNotFoundException.class)
	public void testLoadCart22() throws Throwable {
		FilePersistence testedObject = (FilePersistence) FilePersistence
				.getInstance();
		@SuppressWarnings("unused")
		IShoppingCart result = testedObject.loadCart(256, 920);
		// ClassNotFoundException thrown
	}

	/**
	 * Test for method: loadCart(int,int)
	 * 
	 * @throws Throwable
	 *             Tests may throw any Throwable
	 * @see FilePersistence#loadCart(int,int)
	 * @author White_Birds
	 */
	@Test(expected = java.io.FileNotFoundException.class)
	public void testLoadCart23() throws Throwable {
		FilePersistence testedObject = (FilePersistence) FilePersistence
				.getInstance();
		@SuppressWarnings("unused")
		IShoppingCart result = testedObject.loadCart(858, 2147483647);
		// java.io.FileNotFoundException thrown, originator is possible setup
	}

	/**
	 * Test for method: loadCart(int,int)
	 * 
	 * @throws Throwable
	 *             Tests may throw any Throwable
	 * @see FilePersistence#loadCart(int,int)
	 * @author White_Birds
	 */
	@Test(expected = java.io.StreamCorruptedException.class)
	public void testLoadCart24() throws Throwable {
		FilePersistence testedObject = (FilePersistence) FilePersistence
				.getInstance();
		@SuppressWarnings("unused")
		IShoppingCart result = testedObject.loadCart(256, 920);
		// java.io.StreamCorruptedException thrown
	}

	/**
	 * Test for method: loadCart(int,int)
	 * 
	 * @throws Throwable
	 *             Tests may throw any Throwable
	 * @see FilePersistence#loadCart(int,int)
	 * @author White_Birds
	 */
	@Test(expected = java.io.IOException.class)
	public void testLoadCart25() throws Throwable {
		FilePersistence testedObject = (FilePersistence) FilePersistence
				.getInstance();
		@SuppressWarnings("unused")
		IShoppingCart result = testedObject.loadCart(100, 1000);
		// java.io.IOException thrown
	}

	/**
	 * Test for method: loadCart(int,int)
	 * 
	 * @throws Throwable
	 *             Tests may throw any Throwable
	 * @see FilePersistence#loadCart(int,int)
	 * @author White_Birds
	 */
	@Test(expected = java.io.IOException.class)
	public void testLoadCart26() throws Throwable {
		FilePersistence testedObject = (FilePersistence) FilePersistence
				.getInstance();
		@SuppressWarnings("unused")
		IShoppingCart result = testedObject.loadCart(256, 920);
		// java.io.IOException thrown
	}

	/**
	 * Test for method: loadCart(int,int)
	 * 
	 * @throws Throwable
	 *             Tests may throw any Throwable
	 * @see FilePersistence#loadCart(int,int)
	 * @author White_Birds
	 */
	@Test(expected = java.io.IOException.class)
	public void testLoadCart27() throws Throwable {
		FilePersistence testedObject = (FilePersistence) FilePersistence
				.getInstance();
		@SuppressWarnings("unused")
		IShoppingCart result = testedObject.loadCart(858, 2147483647);
		// java.io.IOException thrown
	}

	/**
	 * Test for method: removeCart(ShoppingCart.IShoppingCart)
	 * 
	 * @throws Throwable
	 *             Tests may throw any Throwable
	 * @see FilePersistence#removeCart(ShoppingCart.IShoppingCart)
	 * @author White_Birds
	 */
	@Test(expected = java.io.FileNotFoundException.class)
	public void testRemoveCart1() throws Throwable {
		FilePersistence testedObject = (FilePersistence) FilePersistence
				.getInstance();
		testedObject.removeCart((IShoppingCart) null);
		// java.io.FileNotFoundException thrown
	}

	/**
	 * Test for method: removeCart(ShoppingCart.IShoppingCart)
	 * 
	 * @throws Throwable
	 *             Tests may throw any Throwable
	 * @see FilePersistence#removeCart(ShoppingCart.IShoppingCart)
	 * @author White_Birds
	 */
	@Test(expected = ClassCastException.class)
	public void testRemoveCart2() throws Throwable {
		FilePersistence testedObject = (FilePersistence) FilePersistence
				.getInstance();
		testedObject.removeCart((IShoppingCart) null);
		// ClassCastException: java.lang.String cannot be cast to
		// ShoppingCart.IShoppingCart thrown
	}

	/**
	 * Test for method: removeCart(ShoppingCart.IShoppingCart)
	 * 
	 * @throws Throwable
	 *             Tests may throw any Throwable
	 * @see FilePersistence#removeCart(ShoppingCart.IShoppingCart)
	 * @author White_Birds
	 */
	@Test(expected = java.lang.NullPointerException.class)
	public void testRemoveCart3() throws Throwable {
		FilePersistence testedObject = (FilePersistence) FilePersistence
				.getInstance();
		testedObject.removeCart((IShoppingCart) null);
		// NullPointerException thrown
	}

	/**
	 * Test for method: removeCart(ShoppingCart.IShoppingCart)
	 * 
	 * @throws Throwable
	 *             Tests may throw any Throwable
	 * @see FilePersistence#removeCart(ShoppingCart.IShoppingCart)
	 * @author White_Birds
	 */
	@Test(expected = java.io.IOException.class)
	public void testRemoveCart4() throws Throwable {
		FilePersistence testedObject = (FilePersistence) FilePersistence
				.getInstance();
		testedObject.removeCart((IShoppingCart) null);
		// java.io.IOException thrown
	}

	/**
	 * Test for method: removeCart(ShoppingCart.IShoppingCart)
	 * 
	 * @throws Throwable
	 *             Tests may throw any Throwable
	 * @see FilePersistence#removeCart(ShoppingCart.IShoppingCart)
	 * @author White_Birds
	 */
	@Test(expected = ClassCastException.class)
	public void testRemoveCart5() throws Throwable {
		FilePersistence testedObject = (FilePersistence) FilePersistence
				.getInstance();
		testedObject.removeCart((IShoppingCart) null);
		// ClassCastException: java.lang.String cannot be cast to
	}

	/**
	 * Test for method: removeCart(ShoppingCart.IShoppingCart)
	 * 
	 * @throws Throwable
	 *             Tests may throw any Throwable
	 * @see FilePersistence#removeCart(ShoppingCart.IShoppingCart)
	 * @author White_Birds
	 */
	@Test(expected = java.io.IOException.class)
	public void testRemoveCart6() throws Throwable {
		FilePersistence testedObject = (FilePersistence) FilePersistence
				.getInstance();
		testedObject.removeCart((IShoppingCart) null);
		// java.io.IOException thrown
	}

	/**
	 * Test for method: removeCart(ShoppingCart.IShoppingCart)
	 * 
	 * @throws Throwable
	 *             Tests may throw any Throwable
	 * @see FilePersistence#removeCart(ShoppingCart.IShoppingCart)
	 * @author White_Birds
	 */
	@Test(expected = ClassNotFoundException.class)
	public void testRemoveCart7() throws Throwable {
		FilePersistence testedObject = (FilePersistence) FilePersistence
				.getInstance();
		testedObject.removeCart((IShoppingCart) null);
		// ClassNotFoundException thrown
	}

	/**
	 * Test for method: removeCart(ShoppingCart.IShoppingCart)
	 * 
	 * @throws Throwable
	 *             Tests may throw any Throwable
	 * @see FilePersistence#removeCart(ShoppingCart.IShoppingCart)
	 * @author White_Birds
	 */
	@Test(expected = java.io.FileNotFoundException.class)
	public void testRemoveCart8() throws Throwable {
		FilePersistence testedObject = (FilePersistence) FilePersistence
				.getInstance();
		testedObject.removeCart((IShoppingCart) null);
		// java.io.FileNotFoundException thrown
	}

	/**
	 * Test for method: removeCart(ShoppingCart.IShoppingCart)
	 * 
	 * @throws Throwable
	 *             Tests may throw any Throwable
	 * @see FilePersistence#removeCart(ShoppingCart.IShoppingCart)
	 * @author White_Birds
	 */
	@Test(expected = java.io.StreamCorruptedException.class)
	public void testRemoveCart9() throws Throwable {
		FilePersistence testedObject = (FilePersistence) FilePersistence
				.getInstance();
		testedObject.removeCart((IShoppingCart) null);
		// java.io.StreamCorruptedException thrown
	}

	/**
	 * Test for method: removeCart(ShoppingCart.IShoppingCart)
	 * 
	 * @throws Throwable
	 *             Tests may throw any Throwable
	 * @see FilePersistence#removeCart(ShoppingCart.IShoppingCart)
	 * @author White_Birds
	 */
	@Test(expected = java.io.IOException.class)
	public void testRemoveCart10() throws Throwable {
		FilePersistence testedObject = (FilePersistence) FilePersistence
				.getInstance();
		testedObject.removeCart((IShoppingCart) null);
		// java.io.IOException thrown
	}

	/**
	 * Test for method: removeCart(ShoppingCart.IShoppingCart)
	 * 
	 * @throws Throwable
	 *             Tests may throw any Throwable
	 * @see FilePersistence#removeCart(ShoppingCart.IShoppingCart)
	 * @author White_Birds
	 */
	public void testRemoveCart11() throws Throwable {
		FilePersistence testedObject = (FilePersistence) FilePersistence
				.getInstance();
		testedObject.removeCart((IShoppingCart) null);
		// No exception thrown
	}

	/**
	 * Test for method: removeCart(ShoppingCart.IShoppingCart)
	 * 
	 * @throws Throwable
	 *             Tests may throw any Throwable
	 * @see FilePersistence#removeCart(ShoppingCart.IShoppingCart)
	 * @author White_Birds
	 */
	@Test(expected = java.io.IOException.class)
	public void testRemoveCart12() throws Throwable {
		FilePersistence testedObject = (FilePersistence) FilePersistence
				.getInstance();
		testedObject.removeCart((IShoppingCart) null);
		// java.io.IOException thrown
	}

	/**
	 * Test for method: removeCart(ShoppingCart.IShoppingCart)
	 * 
	 * @throws Throwable
	 *             Tests may throw any Throwable
	 * @see FilePersistence#removeCart(ShoppingCart.IShoppingCart)
	 * @author White_Birds
	 */
	@Test(expected = java.io.IOException.class)
	public void testRemoveCart13() throws Throwable {
		FilePersistence testedObject = (FilePersistence) FilePersistence
				.getInstance();
		testedObject.removeCart((IShoppingCart) null);
		// java.io.IOException thrown
	}

	/**
	 * Test for method: removeCart(ShoppingCart.IShoppingCart)
	 * 
	 * @throws Throwable
	 *             Tests may throw any Throwable
	 * @see FilePersistence#removeCart(ShoppingCart.IShoppingCart)
	 * @author White_Birds
	 */
	@Test(expected = java.io.FileNotFoundException.class)
	public void testRemoveCart14() throws Throwable {
		FilePersistence testedObject = (FilePersistence) FilePersistence
				.getInstance();
		testedObject.removeCart((IShoppingCart) null);
		// java.io.FileNotFoundException thrown
	}

	/**
	 * Test for method: removeCart(ShoppingCart.IShoppingCart)
	 * 
	 * @throws Throwable
	 *             Tests may throw any Throwable
	 * @see FilePersistence#removeCart(ShoppingCart.IShoppingCart)
	 * @author White_Birds
	 */
	@Test(expected = java.io.IOException.class)
	public void testRemoveCart15() throws Throwable {
		FilePersistence testedObject = (FilePersistence) FilePersistence
				.getInstance();
		testedObject.removeCart((IShoppingCart) null);
		// java.io.IOException thrown
	}

	/**
	 * Test for method: removeCart(ShoppingCart.IShoppingCart)
	 * 
	 * @throws Throwable
	 *             Tests may throw any Throwable
	 * @see FilePersistence#removeCart(ShoppingCart.IShoppingCart)
	 * @author White_Birds
	 */
	@Test(expected = java.io.IOException.class)
	public void testRemoveCart16() throws Throwable {
		FilePersistence testedObject = (FilePersistence) FilePersistence
				.getInstance();
		testedObject.removeCart((IShoppingCart) null);
		// java.io.IOException thrown
	}

	/**
	 * Test for method: save(ShoppingCart.IShoppingCart)
	 * 
	 * @throws Throwable
	 *             Tests may throw any Throwable
	 * @see FilePersistence#save(ShoppingCart.IShoppingCart)
	 * @author White_Birds
	 */
	@Test(expected = java.io.FileNotFoundException.class)
	public void testSave1() throws Throwable {
		FilePersistence testedObject = (FilePersistence) FilePersistence
				.getInstance();
		testedObject.save((IShoppingCart) null);
		// java.io.FileNotFoundException thrown
	}

	/**
	 * Test for method: save(ShoppingCart.IShoppingCart)
	 * 
	 * @throws Throwable
	 *             Tests may throw any Throwable
	 * @see FilePersistence#save(ShoppingCart.IShoppingCart)
	 * @author White_Birds
	 */
	@Test(expected = ClassCastException.class)
	public void testSave2() throws Throwable {
		FilePersistence testedObject = (FilePersistence) FilePersistence
				.getInstance();
		testedObject.save((IShoppingCart) null);
		// ClassCastException: java.lang.String cannot be cast to
		// ShoppingCart.IShoppingCart thrown
	}

	/**
	 * Test for method: save(ShoppingCart.IShoppingCart)
	 * 
	 * @throws Throwable
	 *             Tests may throw any Throwable
	 * @see FilePersistence#save(ShoppingCart.IShoppingCart)
	 * @author White_Birds
	 */
	@Test(expected = java.lang.NullPointerException.class)
	public void testSave3() throws Throwable {
		FilePersistence testedObject = (FilePersistence) FilePersistence
				.getInstance();
		testedObject.save((IShoppingCart) null);
		// NullPointerException thrown
	}

	/**
	 * Test for method: save(ShoppingCart.IShoppingCart)
	 * 
	 * @throws Throwable
	 *             Tests may throw any Throwable
	 * @see FilePersistence#save(ShoppingCart.IShoppingCart)
	 * @author White_Birds
	 */
	@Test(expected = java.io.IOException.class)
	public void testSave4() throws Throwable {
		FilePersistence testedObject = (FilePersistence) FilePersistence
				.getInstance();
		testedObject.save((IShoppingCart) null);
		// java.io.IOException thrown
	}

	/**
	 * Test for method: save(ShoppingCart.IShoppingCart)
	 * 
	 * @throws Throwable
	 *             Tests may throw any Throwable
	 * @see FilePersistence#save(ShoppingCart.IShoppingCart)
	 * @author White_Birds
	 */
	@Test(expected = ClassCastException.class)
	public void testSave5() throws Throwable {
		FilePersistence testedObject = (FilePersistence) FilePersistence
				.getInstance();
		testedObject.save((IShoppingCart) null);
		// ClassCastException: java.lang.String cannot be cast to
		// java.util.ArrayList thrown
	}

	/**
	 * Test for method: save(ShoppingCart.IShoppingCart)
	 * 
	 * @throws Throwable
	 *             Tests may throw any Throwable
	 * @see FilePersistence#save(ShoppingCart.IShoppingCart)
	 * @author White_Birds
	 */
	@Test(expected = java.io.IOException.class)
	public void testSave6() throws Throwable {
		FilePersistence testedObject = (FilePersistence) FilePersistence
				.getInstance();
		testedObject.save((IShoppingCart) null);
		// java.io.IOException thrown
	}

	/**
	 * Test for method: save(ShoppingCart.IShoppingCart)
	 * 
	 * @throws Throwable
	 *             Tests may throw any Throwable
	 * @see FilePersistence#save(ShoppingCart.IShoppingCart)
	 * @author White_Birds
	 */
	@Test(expected = ClassNotFoundException.class)
	public void testSave7() throws Throwable {
		FilePersistence testedObject = (FilePersistence) FilePersistence
				.getInstance();
		testedObject.save((IShoppingCart) null);
		// ClassNotFoundException thrown
	}

	/**
	 * Test for method: save(ShoppingCart.IShoppingCart)
	 * 
	 * @throws Throwable
	 *             Tests may throw any Throwable
	 * @see FilePersistence#save(ShoppingCart.IShoppingCart)
	 * @author White_Birds
	 */
	@Test(expected = java.io.FileNotFoundException.class)
	public void testSave8() throws Throwable {
		FilePersistence testedObject = (FilePersistence) FilePersistence
				.getInstance();
		testedObject.save((IShoppingCart) null);
		// java.io.FileNotFoundException thrown
	}

	/**
	 * Test for method: save(ShoppingCart.IShoppingCart)
	 * 
	 * @throws Throwable
	 *             Tests may throw any Throwable
	 * @see FilePersistence#save(ShoppingCart.IShoppingCart)
	 * @author White_Birds
	 */
	@Test(expected = java.io.StreamCorruptedException.class)
	public void testSave9() throws Throwable {
		FilePersistence testedObject = (FilePersistence) FilePersistence
				.getInstance();
		testedObject.save((IShoppingCart) null);
		// java.io.StreamCorruptedException thrown
	}

	/**
	 * Test for method: save(ShoppingCart.IShoppingCart)
	 * 
	 * @throws Throwable
	 *             Tests may throw any Throwable
	 * @see FilePersistence#save(ShoppingCart.IShoppingCart)
	 * @author White_Birds
	 */
	@Test(expected = java.io.IOException.class)
	public void testSave10() throws Throwable {
		FilePersistence testedObject = (FilePersistence) FilePersistence
				.getInstance();
		testedObject.save((IShoppingCart) null);
		// java.io.IOException thrown
	}

	/**
	 * Test for method: save(ShoppingCart.IShoppingCart)
	 * 
	 * @throws Throwable
	 *             Tests may throw any Throwable
	 * @see FilePersistence#save(ShoppingCart.IShoppingCart)
	 * @author White_Birds
	 */
	@Test(expected = java.lang.NullPointerException.class)
	public void testSave11() throws Throwable {
		FilePersistence testedObject = (FilePersistence) FilePersistence
				.getInstance();
		testedObject.save((IShoppingCart) null);
		// No exception thrown
	}

	/**
	 * Test for method: save(ShoppingCart.IShoppingCart)
	 * 
	 * @throws Throwable
	 *             Tests may throw any Throwable
	 * @see FilePersistence#save(ShoppingCart.IShoppingCart)
	 * @author White_Birds
	 */
	@Test(expected = java.io.IOException.class)
	public void testSave12() throws Throwable {
		FilePersistence testedObject = (FilePersistence) FilePersistence
				.getInstance();
		testedObject.save((IShoppingCart) null);
		// java.io.IOException thrown
	}

	/**
	 * Test for method: save(ShoppingCart.IShoppingCart)
	 * 
	 * @throws Throwable
	 *             Tests may throw any Throwable
	 * @see FilePersistence#save(ShoppingCart.IShoppingCart)
	 * @author White_Birds
	 */
	@Test(expected = java.io.IOException.class)
	public void testSave13() throws Throwable {
		FilePersistence testedObject = (FilePersistence) FilePersistence
				.getInstance();
		testedObject.save((IShoppingCart) null);
		// java.io.IOException thrown
	}

	/**
	 * Test for method: save(ShoppingCart.IShoppingCart)
	 * 
	 * @throws Throwable
	 *             Tests may throw any Throwable
	 * @see FilePersistence#save(ShoppingCart.IShoppingCart)
	 * @author White_Birds
	 */
	@Test(expected = java.io.FileNotFoundException.class)
	public void testSave14() throws Throwable {
		FilePersistence testedObject = (FilePersistence) FilePersistence
				.getInstance();
		testedObject.save((IShoppingCart) null);
		// java.io.FileNotFoundException thrown
	}

	/**
	 * Test for method: save(ShoppingCart.IShoppingCart)
	 * 
	 * @throws Throwable
	 *             Tests may throw any Throwable
	 * @see FilePersistence#save(ShoppingCart.IShoppingCart)
	 * @author White_Birds
	 */
	@Test(expected = java.io.IOException.class)
	public void testSave15() throws Throwable {
		FilePersistence testedObject = (FilePersistence) FilePersistence
				.getInstance();
		testedObject.save((IShoppingCart) null);
		// java.io.IOException thrown
	}

	/**
	 * Test for method: save(ShoppingCart.IShoppingCart)
	 * 
	 * @throws Throwable
	 *             Tests may throw any Throwable
	 * @see FilePersistence#save(ShoppingCart.IShoppingCart)
	 * @author White_Birds
	 */
	@Test(expected = java.io.IOException.class)
	public void testSave16() throws Throwable {
		FilePersistence testedObject = (FilePersistence) FilePersistence
				.getInstance();
		testedObject.save((IShoppingCart) null);
		// java.io.IOException thrown
	}

	/**
	 * Utility main method. Runs the test cases defined in this test class.
	 * 
	 * Usage: java FilePersistenceTest
	 * 
	 * @param args
	 *            command line arguments are not needed
	 * @author White_Birds
	 */
	public static void main(String[] args) {
		Result result = JUnitCore.runClasses(FilePersistence.class);
		for (Failure failure : result.getFailures()) {
			System.err.println(failure.toString());
			System.err.println();
		}
		System.out.println(result.wasSuccessful());
	}
}