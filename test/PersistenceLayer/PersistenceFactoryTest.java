/*
 * Copyright (C) 2014 White_Birds Open Source Project 
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at 
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software 
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and 
 * limitations under the License.
 */

package PersistenceLayer;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import org.junit.Test;
import org.junit.runner.JUnitCore;
import org.junit.runner.Result;
import org.junit.runner.notification.Failure;

/**
 * PersistenceFactoryTest is a unit test class for class PersistenceFactory
 * 
 * @see PersistenceLayer.PersistenceFactory
 * @author White_Birds
 */
public class PersistenceFactoryTest {
	/**
	 * Test for method: loadMechanism(java.lang.String)
	 * 
	 * @throws Throwable
	 *             Tests may throw any Throwable
	 * @see PersistenceFactory#loadMechanism(java.lang.String)
	 * @author White_Birds
	 */
	@Test
	public void testLoadMechanism1() throws Throwable {
		PersistenceFactory testedObject = new PersistenceFactory();
		IPersistenceMechanism result = testedObject.loadMechanism("SQLPersis");
		assertEquals(null, result);
		// No exception thrown
	}

	/**
	 * Test for method: loadMechanism(java.lang.String)
	 * 
	 * @throws Throwable
	 *             Tests may throw any Throwable
	 * @see PersistenceFactory#loadMechanism(java.lang.String)
	 * @author White_Birds
	 */
	@Test
	public void testLoadMechanism2() throws Throwable {
		PersistenceFactory testedObject = new PersistenceFactory();
		IPersistenceMechanism result = testedObject
				.loadMechanism("FilePersistence");
		assertNotNull(result);
		// No exception thrown
	}

	/**
	 * Test for method: loadMechanism(java.lang.String)
	 * 
	 * @throws Throwable
	 *             Tests may throw any Throwable
	 * @see PersistenceFactory#loadMechanism(java.lang.String)
	 * @author White_Birds
	 */
	@Test(expected = java.lang.NullPointerException.class)
	public void testLoadMechanism3() throws Throwable {
		PersistenceFactory testedObject = new PersistenceFactory();
		@SuppressWarnings("unused")
		IPersistenceMechanism result = testedObject
				.loadMechanism((String) null);
		// NullPointerException thrown
	}

	/**
	 * Test for method: loadMechanism(java.lang.String)
	 * 
	 * @throws Throwable
	 *             Tests may throw any Throwable
	 * @see PersistenceFactory#loadMechanism(java.lang.String)
	 * @author White_Birds
	 */
	@Test
	public void testLoadMechanism4() throws Throwable {
		PersistenceFactory testedObject = new PersistenceFactory();
		@SuppressWarnings("unused")
		IPersistenceMechanism result = testedObject
				.loadMechanism("SQLPersistence");
		// java.sql.SQLException thrown
	}

	/**
	 * Test for method: loadMechanism(java.lang.String)
	 * 
	 * @throws Throwable
	 *             Tests may throw any Throwable
	 * @see PersistenceFactory#loadMechanism(java.lang.String)
	 * @author White_Birds
	 */
	@Test
	public void testLoadMechanism5() throws Throwable {
		PersistenceFactory testedObject = new PersistenceFactory();
		@SuppressWarnings("unused")
		IPersistenceMechanism result = testedObject
				.loadMechanism("SQLPersistence");
		// ClassNotFoundException thrown
	}

	/**
	 * Test for method: PersistenceFactory()
	 * 
	 * @throws Throwable
	 *             Tests may throw any Throwable
	 * @see PersistenceFactory#PersistenceFactory()
	 * @author White_Birds
	 */
	@Test
	public void testPersistenceFactory() throws Throwable {
		@SuppressWarnings("unused")
		PersistenceFactory testedObject = new PersistenceFactory();
		// No exception thrown
	}

	/**
	 * Utility main method. Runs the test cases defined in this test class.
	 * 
	 * Usage: java PersistenceFactoryTest
	 * 
	 * @param args
	 *            command line arguments are not needed
	 * @author White_Birds
	 */
	public static void main(String[] args) {
		Result result = JUnitCore.runClasses(PersistenceFactory.class);
		for (Failure failure : result.getFailures()) {
			System.err.println(failure.toString());
			System.err.println();
		}
		System.out.println(result.wasSuccessful());
	}
}