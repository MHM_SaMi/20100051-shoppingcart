/*
 * Copyright (C) 2014 White_Birds Open Source Project 
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at 
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software 
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and 
 * limitations under the License.
 */

package PersistenceLayer;

import org.junit.Test;
import org.junit.runner.JUnitCore;
import org.junit.runner.Result;
import org.junit.runner.notification.Failure;

import Exception.SQL_Exception;
import ShoppingCart.IShoppingCart;

/**
 * SQLPersistenceTest is a unit test class for class SQLPersistence
 * 
 * @see PersistenceLayer.SQLPersistence
 * @author White_Birds
 */
public class SQLPersistenceTest {

	/**
	 * Test for method: createCart(int,int)
	 * 
	 * @throws Throwable
	 *             Tests may throw any Throwable
	 * @see SQLPersistence#createCart(int,int)
	 * @author White_Birds
	 */
	@Test
	public void testCreateCart1() throws Throwable {
		SQLPersistence testedObject = (SQLPersistence) SQLPersistence
				.getInstance();
		@SuppressWarnings("unused")
		IShoppingCart result = testedObject.createCart(858, 2147483647);
		// java.sql.SQLException thrown
	}

	/**
	 * Test for method: createCart(int,int)
	 * 
	 * @throws Throwable
	 *             Tests may throw any Throwable
	 * @see SQLPersistence#createCart(int,int)
	 * @author White_Birds
	 */
	@Test
	public void testCreateCart2() throws Throwable {
		SQLPersistence testedObject = (SQLPersistence) SQLPersistence
				.getInstance();
		@SuppressWarnings("unused")
		IShoppingCart result = testedObject.createCart(5, -10);
		// java.sql.SQLException thrown
	}

	/**
	 * Test for method: getInstance()
	 * 
	 * @throws Throwable
	 *             Tests may throw any Throwable
	 * @see SQLPersistence#getInstance()
	 * @author White_Birds
	 */
	@Test
	public void testGetInstance1() throws Throwable {
		@SuppressWarnings("unused")
		IPersistenceMechanism result = SQLPersistence.getInstance();
	}

	/**
	 * Test for method: loadCart(int)
	 * 
	 * @throws Throwable
	 *             Tests may throw any Throwable
	 * @see SQLPersistence#loadCart(int)
	 * @author White_Birds
	 */
	@Test(expected = SQL_Exception.class)
	public void testLoadCart1() throws Throwable {
		SQLPersistence testedObject = (SQLPersistence) SQLPersistence
				.getInstance();
		@SuppressWarnings("unused")
		IShoppingCart result = testedObject.loadCart(1000);
		// java.sql.SQLException thrown
	}

	/**
	 * Test for method: loadCart(int)
	 * 
	 * @throws Throwable
	 *             Tests may throw any Throwable
	 * @see SQLPersistence#loadCart(int)
	 * @author White_Birds
	 */
	@Test(expected = SQL_Exception.class)
	public void testLoadCart2() throws Throwable {
		SQLPersistence testedObject = (SQLPersistence) SQLPersistence
				.getInstance();
		@SuppressWarnings("unused")
		IShoppingCart result = testedObject.loadCart(5);
		// java.sql.SQLException thrown
	}

	/**
	 * Test for method: loadCart(int,int)
	 * 
	 * @throws Throwable
	 *             Tests may throw any Throwable
	 * @see SQLPersistence#loadCart(int,int)
	 * @author White_Birds
	 */
	@Test
	public void testLoadCart3() throws Throwable {
		SQLPersistence testedObject = (SQLPersistence) SQLPersistence
				.getInstance();
		@SuppressWarnings("unused")
		IShoppingCart result = testedObject.loadCart(858, 2147483647);
		// java.sql.SQLException thrown
	}

	/**
	 * Test for method: loadCart(int,int)
	 * 
	 * @throws Throwable
	 *             Tests may throw any Throwable
	 * @see SQLPersistence#loadCart(int,int)
	 * @author White_Birds
	 */
	@Test
	public void testLoadCart4() throws Throwable {
		SQLPersistence testedObject = (SQLPersistence) SQLPersistence
				.getInstance();
		@SuppressWarnings("unused")
		IShoppingCart result = testedObject.loadCart(5, -10);
		// java.sql.SQLException thrown
	}

	/**
	 * Test for method: removeCart(ShoppingCart.IShoppingCart)
	 * 
	 * @throws Throwable
	 *             Tests may throw any Throwable
	 * @see SQLPersistence#removeCart(ShoppingCart.IShoppingCart)
	 * @author White_Birds
	 */
	@Test(expected = java.lang.NullPointerException.class)
	public void testRemoveCart1() throws Throwable {
		SQLPersistence testedObject = (SQLPersistence) SQLPersistence
				.getInstance();
		testedObject.removeCart((IShoppingCart) null);
		// NullPointerException thrown, originator is arg 1 to <Method
	}

	/**
	 * Test for method: removeCart(ShoppingCart.IShoppingCart)
	 * 
	 * @throws Throwable
	 *             Tests may throw any Throwable
	 * @see SQLPersistence#removeCart(ShoppingCart.IShoppingCart)
	 * @author White_Birds
	 */
	@Test(expected = java.lang.NullPointerException.class)
	public void testRemoveCart2() throws Throwable {
		SQLPersistence testedObject = (SQLPersistence) SQLPersistence
				.getInstance();
		testedObject.removeCart((IShoppingCart) null);
		// java.lang.NullPointerException
	}

	/**
	 * Test for method: save(ShoppingCart.IShoppingCart)
	 * 
	 * @throws Throwable
	 *             Tests may throw any Throwable
	 * @see SQLPersistence#save(ShoppingCart.IShoppingCart)
	 * @author White_Birds
	 */
	@Test(expected = java.lang.NullPointerException.class)
	public void testSave1() throws Throwable {
		SQLPersistence testedObject = (SQLPersistence) SQLPersistence
				.getInstance();
		testedObject.save((IShoppingCart) null);
		// NullPointerException thrown, originator is arg 1 to <Method
	}

	/**
	 * Test for method: save(ShoppingCart.IShoppingCart)
	 * 
	 * @throws Throwable
	 *             Tests may throw any Throwable
	 * @see SQLPersistence#save(ShoppingCart.IShoppingCart)
	 * @author White_Birds
	 */
	@Test(expected = java.lang.NullPointerException.class)
	public void testSave2() throws Throwable {
		SQLPersistence testedObject = (SQLPersistence) SQLPersistence
				.getInstance();
		testedObject.save((IShoppingCart) null);
		// java.lang.NullPointerException
	}

	/**
	 * Utility main method. Runs the test cases defined in this test class.
	 * 
	 * Usage: java SQLPersistenceTest
	 * 
	 * @param args
	 *            command line arguments are not needed
	 * @author White_Birds
	 */
	public static void main(String[] args) {
		Result result = JUnitCore.runClasses(SQLPersistence.class);
		for (Failure failure : result.getFailures()) {
			System.err.println(failure.toString());
			System.err.println();
		}
		System.out.println(result.wasSuccessful());
	}
}