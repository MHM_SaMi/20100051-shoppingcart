/*
 * Copyright (C) 2014 White_Birds Open Source Project 
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at 
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software 
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and 
 * limitations under the License.
 */

package ShoppingCart;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.util.ArrayList;
import java.util.Date;

import org.junit.Test;
import org.junit.runner.JUnitCore;
import org.junit.runner.Result;
import org.junit.runner.notification.Failure;

import Exception.CartItemNotFoundException;

/**
 * ShoppingCartTest is a unit test class for class ShoppingCart
 * 
 * @see ShoppingCart.ShoppingCart
 * @author White_Birds
 */
public class ShoppingCartTest {

	/**
	 * Test for method: countItems()
	 * 
	 * @throws Throwable
	 *             Tests may throw any Throwable
	 * @see ShoppingCart#countItems()
	 * @author White_Birds
	 */
	@Test
	public void testCountItems1() throws Throwable {
		ShoppingCart testedObject = new ShoppingCart(-2147483648, -1000, 256);
		testedObject.addItem((ICartItem) null);
		int result = testedObject.countItems();
		assertEquals(1, result); // jtest_unverified
		assertEquals(-2147483648, testedObject.getId());
		assertNotNull(testedObject.getLastAccessedDate());
		assertEquals(new java.util.Date().toString(), testedObject
				.getLastAccessedDate().toString());
		assertNotNull(testedObject.getItems());
		assertEquals("[null]", testedObject.getItems().toString());
		assertEquals(256, testedObject.getSessionID());
		assertEquals(-1000, testedObject.getCustomerID());
		// No exception thrown
	}

	/**
	 * Test for method: countItems()
	 * 
	 * @throws Throwable
	 *             Tests may throw any Throwable
	 * @see ShoppingCart#countItems()
	 * @author White_Birds
	 */
	@Test
	public void testCountItems2() throws Throwable {
		ShoppingCart testedObject = new ShoppingCart(5, -10, 100);
		int result = testedObject.countItems();
		assertEquals(0, result);
		assertEquals(5, testedObject.getId());
		assertNotNull(testedObject.getLastAccessedDate());
		assertEquals(new java.util.Date().toString(), testedObject
				.getLastAccessedDate().toString());
		assertNotNull(testedObject.getItems());
		assertEquals(0, testedObject.getItems().size());
		assertEquals(100, testedObject.getSessionID());
		assertEquals(-10, testedObject.getCustomerID());
		// No exception thrown
	}

	/**
	 * Test for method: getCustomerID()
	 * 
	 * @throws Throwable
	 *             Tests may throw any Throwable
	 * @see ShoppingCart#getCustomerID()
	 * @author White_Birds
	 */
	@Test
	public void testGetCustomerID() throws Throwable {
		ShoppingCart testedObject = new ShoppingCart(5, 0, 100);
		int result = testedObject.getCustomerID();
		assertEquals(0, result);
		// No exception thrown
	}

	/**
	 * Test for method: getId()
	 * 
	 * @throws Throwable
	 *             Tests may throw any Throwable
	 * @see ShoppingCart#getId()
	 * @author White_Birds
	 */
	@Test
	public void testGetId() throws Throwable {
		ShoppingCart testedObject = new ShoppingCart(0, -10, 100);
		int result = testedObject.getId();
		assertEquals(0, result);
		// No exception thrown
	}

	/**
	 * Test for method: getItem(int)
	 * 
	 * @throws Throwable
	 *             Tests may throw any Throwable
	 * @see ShoppingCart#getItem(int)
	 * @author White_Birds
	 */
	@Test(expected = CartItemNotFoundException.class)
	public void testGetItem() throws Throwable {
		ShoppingCart testedObject = new ShoppingCart(256, 920, 5);
		CartItem testedObjectItem = new CartItem(-10, 100, 1, 4.1E-4);
		testedObject.addItem((ICartItem) testedObjectItem);
		@SuppressWarnings("unused")
		ICartItem result = testedObject.getItem(-10);
		// CartItemNotFoundException thrown
	}

	/**
	 * Test for method: getItems()
	 * 
	 * @throws Throwable
	 *             Tests may throw any Throwable
	 * @see ShoppingCart#getItems()
	 * @author White_Birds
	 */
	@Test
	public void testGetItems() throws Throwable {
		ShoppingCart testedObject = new ShoppingCart(5, -10, 100);
		ArrayList<ICartItem> result = testedObject.getItems();
		assertNotNull(result);
		assertEquals(0, result.size());
		// No exception thrown
	}

	/**
	 * Test for method: getLastAccessedDate()
	 * 
	 * @throws Throwable
	 *             Tests may throw any Throwable
	 * @see ShoppingCart#getLastAccessedDate()
	 * @author White_Birds
	 */
	@Test
	public void testGetLastAccessedDate() throws Throwable {
		ShoppingCart testedObject = new ShoppingCart(5, -10, 100);
		Date result = testedObject.getLastAccessedDate();
		assertNotNull(result);
		assertEquals(new java.util.Date().toString(), result.toString());
		// No exception thrown
	}

	/**
	 * Test for method: getSessionID()
	 * 
	 * @throws Throwable
	 *             Tests may throw any Throwable
	 * @see ShoppingCart#getSessionID()
	 * @author White_Birds
	 */
	@Test
	public void testGetSessionID() throws Throwable {
		ShoppingCart testedObject = new ShoppingCart(5, -10, 0);
		int result = testedObject.getSessionID();
		assertEquals(0, result);
		// No exception thrown
	}

	/**
	 * Test for method: removeItem(int)
	 * 
	 * @throws Throwable
	 *             Tests may throw any Throwable
	 * @see ShoppingCart#removeItem(int)
	 * @author White_Birds
	 */
	@Test(expected = CartItemNotFoundException.class)
	public void testRemoveItem() throws Throwable {
		ShoppingCart testedObject = new ShoppingCart(256, 920, 5);
		CartItem testedObjectItem = new CartItem(2147483647, -2147483648, 1,
				1000.001);
		testedObject.addItem((ICartItem) testedObjectItem);
		testedObject.removeItem(-10);
		// CartItemNotFoundException thrown
	}

	/**
	 * Test for method: ShoppingCart(int,int,int)
	 * 
	 * @throws Throwable
	 *             Tests may throw any Throwable
	 * @see ShoppingCart#ShoppingCart(int,int,int)
	 * @author White_Birds
	 */
	@Test
	public void testShoppingCart1() throws Throwable {
		ShoppingCart testedObject = new ShoppingCart(1000, 858, 2147483647);
		assertEquals(1000, testedObject.getId());
		assertNotNull(testedObject.getLastAccessedDate());
		assertEquals(new java.util.Date().toString(), testedObject
				.getLastAccessedDate().toString());
		assertNotNull(testedObject.getItems());
		assertEquals(0, testedObject.getItems().size());
		assertEquals(2147483647, testedObject.getSessionID());
		assertEquals(858, testedObject.getCustomerID());
		// No exception thrown
	}

	/**
	 * Test for method: ShoppingCart(int,int,int)
	 * 
	 * @throws Throwable
	 *             Tests may throw any Throwable
	 * @see ShoppingCart#ShoppingCart(int,int,int)
	 * @author White_Birds
	 */
	@Test
	public void testShoppingCart2() throws Throwable {
		ShoppingCart testedObject = new ShoppingCart(5, -10, 100);
		assertEquals(5, testedObject.getId());
		assertNotNull(testedObject.getLastAccessedDate());
		assertEquals(new java.util.Date().toString(), testedObject
				.getLastAccessedDate().toString());
		assertNotNull(testedObject.getItems());
		assertEquals(0, testedObject.getItems().size());
		assertEquals(100, testedObject.getSessionID());
		assertEquals(-10, testedObject.getCustomerID());
		// No exception thrown
	}

	/**
	 * Test for method: updateQuantity(int,int)
	 * 
	 * @throws Throwable
	 *             Tests may throw any Throwable
	 * @see ShoppingCart#updateQuantity(int,int)
	 * @author White_Birds
	 */
	@Test(expected = CartItemNotFoundException.class)
	public void testUpdateQuantity() throws Throwable {
		ShoppingCart testedObject = new ShoppingCart(5, -10, 100);
		CartItem testedObjectItem = new CartItem(920, 5, 1, 0.999);
		testedObject.addItem((ICartItem) testedObjectItem);
		testedObject.updateQuantity(1000, 858);
		// CartItemNotFoundException thrown
	}

	/**
	 * Utility main method. Runs the test cases defined in this test class.
	 * 
	 * Usage: java ShoppingCartTest
	 * 
	 * @param args
	 *            command line arguments are not needed
	 * @author White_Birds
	 */
	public static void main(String[] args) {
		Result result = JUnitCore.runClasses(ShoppingCart.class);
		for (Failure failure : result.getFailures()) {
			System.err.println(failure.toString());
			System.err.println();
		}
		System.out.println(result.wasSuccessful());
	}
}