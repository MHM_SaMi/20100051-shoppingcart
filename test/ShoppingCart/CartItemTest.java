/*
 * Copyright (C) 2014 White_Birds Open Source Project 
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at 
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software 
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and 
 * limitations under the License.
 */

package ShoppingCart;

import static org.junit.Assert.assertEquals;

import org.junit.Test;
import org.junit.runner.JUnitCore;
import org.junit.runner.Result;
import org.junit.runner.notification.Failure;

import Exception.IlegalQuantityException;

/**
 * CartItemTest is a unit test class for class CartItem
 * 
 * @see ShoppingCart.CartItem
 * @author White_Birds
 */
public class CartItemTest {
	/**
	 * Test for method: CartItem(int,int,int,double)
	 * 
	 * @throws Throwable
	 *             Tests may throw any Throwable
	 * @see CartItem#CartItem(int,int,int,double)
	 * @author White_Birds
	 */
	@Test
	public void testCartItem1() throws Throwable {
		CartItem testedObject = new CartItem(256, 920, 5, 4.5);
		testedObject.setId(-10);
		testedObject.setProductId(100);
		testedObject.setQuantity(1000);
		testedObject.setUnitPrice(-17.13);
		assertEquals(-10, testedObject.getId());
		assertEquals(-17130.0, testedObject.getTotalCost(), 0.0);
		assertEquals(1000, testedObject.getQuantity());
		assertEquals(-17.13, testedObject.getUnitPrice(), 0.0);
		assertEquals(100, testedObject.getProductId());
		// No exception thrown
	}

	/**
	 * Test for method: CartItem(int,int,int,double)
	 * 
	 * @throws Throwable
	 *             Tests may throw any Throwable
	 * @see CartItem#CartItem(int,int,int,double)
	 * @author White_Birds
	 */
	@Test(expected = IlegalQuantityException.class)
	public void testCartItem2() throws Throwable {
		CartItem testedObject = new CartItem(920, 5, 1, 0.999);
		testedObject.setId(100);
		testedObject.setProductId(1000);
		testedObject.setQuantity(0);
		// Exception.IlegalQuantityException thrown
	}

	/**
	 * Test for method: CartItem(int,int,int,double)
	 * 
	 * @throws Throwable
	 *             Tests may throw any Throwable
	 * @see CartItem#CartItem(int,int,int,double)
	 * @author White_Birds
	 */
	@Test(expected = IlegalQuantityException.class)
	public void testCartItem3() throws Throwable {
		CartItem testedObject = new CartItem(-10, 100, 1, 4.1E-4);
		testedObject.setId(858);
		testedObject.setProductId(2147483647);
		testedObject.setQuantity(0);
		// Exception.IlegalQuantityException thrown
	}

	/**
	 * Test for method: CartItem(int,int,int,double)
	 * 
	 * @throws Throwable
	 *             Tests may throw any Throwable
	 * @see CartItem#CartItem(int,int,int,double)
	 * @author White_Birds
	 */
	@Test(expected = IlegalQuantityException.class)
	public void testCartItem4() throws Throwable {
		CartItem testedObject = new CartItem(2147483647, -2147483648, 1,
				1000.001);
		testedObject.setId(256);
		testedObject.setProductId(920);
		testedObject.setQuantity(0);
		// Exception.IlegalQuantityException thrown
	}

	/**
	 * Test for method: CartItem(int,int,int,double)
	 * 
	 * @throws Throwable
	 *             Tests may throw any Throwable
	 * @see CartItem#CartItem(int,int,int,double)
	 * @author White_Birds
	 */
	@Test(expected = IlegalQuantityException.class)
	public void testCartItem5() throws Throwable {
		CartItem testedObject = new CartItem(1000, 858, 1, 1000.0);
		testedObject.setId(-2147483648);
		testedObject.setProductId(-1000);
		testedObject.setQuantity(0);
		// Exception.IlegalQuantityException thrown
	}

	/**
	 * Test for method: CartItem(int,int,int,double)
	 * 
	 * @throws Throwable
	 *             Tests may throw any Throwable
	 * @see CartItem#CartItem(int,int,int,double)
	 * @author White_Birds
	 */
	@Test
	public void testCartItem6() throws Throwable {
		CartItem testedObject = new CartItem(1000, 858, 1, -17.13);
		testedObject.setId(-2147483648);
		testedObject.setProductId(-1000);
		testedObject.setQuantity(1);
		testedObject.setUnitPrice(1000.001);
		assertEquals(-2147483648, testedObject.getId());
		assertEquals(1000.001, testedObject.getTotalCost(), 0.0);
		assertEquals(1, testedObject.getQuantity());
		assertEquals(1000.001, testedObject.getUnitPrice(), 0.0);
		assertEquals(-1000, testedObject.getProductId());
		// No exception thrown
	}

	/**
	 * Test for method: CartItem(int,int,int,double)
	 * 
	 * @throws Throwable
	 *             Tests may throw any Throwable
	 * @see CartItem#CartItem(int,int,int,double)
	 * @author White_Birds
	 */
	@Test
	public void testCartItem7() throws Throwable {
		CartItem testedObject = new CartItem(-2147483648, -1000, 256, 1000.001);
		assertEquals(-2147483648, testedObject.getId());
		assertEquals(256000.256, testedObject.getTotalCost(), 0.0);
		assertEquals(256, testedObject.getQuantity());
		assertEquals(1000.001, testedObject.getUnitPrice(), 0.0);
		assertEquals(-1000, testedObject.getProductId());
		// No exception thrown
	}

	/**
	 * Test for method: CartItem(int,int,int,double)
	 * 
	 * @throws Throwable
	 *             Tests may throw any Throwable
	 * @see CartItem#CartItem(int,int,int,double)
	 * @author White_Birds
	 */
	@Test(expected = IlegalQuantityException.class)
	public void testCartItem8() throws Throwable {
		@SuppressWarnings("unused")
		CartItem testedObject = new CartItem(858, 2147483647, 0, 0.999);
		// Exception.IlegalQuantityException thrown
	}

	/**
	 * Test for method: CartItem(int,int,int,double)
	 * 
	 * @throws Throwable
	 *             Tests may throw any Throwable
	 * @see CartItem#CartItem(int,int,int,double)
	 * @author White_Birds
	 */
	@Test(expected = IlegalQuantityException.class)
	public void testCartItem9() throws Throwable {
		@SuppressWarnings("unused")
		CartItem testedObject = new CartItem(-10, 100, 0, 99.99999999);
		// Exception.IlegalQuantityException thrown
	}

	/**
	 * Test for method: CartItem(int,int,int,double)
	 * 
	 * @throws Throwable
	 *             Tests may throw any Throwable
	 * @see CartItem#CartItem(int,int,int,double)
	 * @author White_Birds
	 */
	@Test(expected = IlegalQuantityException.class)
	public void testCartItem10() throws Throwable {
		@SuppressWarnings("unused")
		CartItem testedObject = new CartItem(2147483647, -2147483648, 0,
				4.3E-307);
		// Exception.IlegalQuantityException thrown
	}

	/**
	 * Test for method: CartItem(int,int,int,double)
	 * 
	 * @throws Throwable
	 *             Tests may throw any Throwable
	 * @see CartItem#CartItem(int,int,int,double)
	 * @author White_Birds
	 */
	@Test(expected = IlegalQuantityException.class)
	public void testCartItem11() throws Throwable {
		@SuppressWarnings("unused")
		CartItem testedObject = new CartItem(920, 5, 0, 4.1E-4);
		// Exception.IlegalQuantityException thrown
	}

	/**
	 * Test for method: CartItem(int,int,int,double)
	 * 
	 * @throws Throwable
	 *             Tests may throw any Throwable
	 * @see CartItem#CartItem(int,int,int,double)
	 * @author White_Birds
	 */
	@Test(expected = IlegalQuantityException.class)
	public void testCartItem12() throws Throwable {
		@SuppressWarnings("unused")
		CartItem testedObject = new CartItem(1000, 858, 0, -17.13);
		// Exception.IlegalQuantityException thrown
	}

	/**
	 * Test for method: CartItem(int,int,int,double)
	 * 
	 * @throws Throwable
	 *             Tests may throw any Throwable
	 * @see CartItem#CartItem(int,int,int,double)
	 * @author White_Birds
	 */
	@Test
	public void testCartItem13() throws Throwable {
		CartItem testedObject = new CartItem(5, -10, 1, 4.5);
		assertEquals(5, testedObject.getId());
		assertEquals(4.5, testedObject.getTotalCost(), 0.0);
		assertEquals(1, testedObject.getQuantity());
		assertEquals(4.5, testedObject.getUnitPrice(), 0.0);
		assertEquals(-10, testedObject.getProductId());
		// No exception thrown
	}

	/**
	 * Test for method: getTotalCost()
	 * 
	 * @throws Throwable
	 *             Tests may throw any Throwable
	 * @see CartItem#getTotalCost()
	 * @author White_Birds
	 */
	@Test
	public void testGetTotalCost1() throws Throwable {
		CartItem testedObject = new CartItem(256, 920, 5, 3.1415);
		double result = testedObject.getTotalCost();
		assertEquals(15.707500000000001, result, 0.0);
		// No exception thrown
	}

	/**
	 * Test for method: getTotalCost()
	 * 
	 * @throws Throwable
	 *             Tests may throw any Throwable
	 * @see CartItem#getTotalCost()
	 * @author White_Birds
	 */
	@Test
	public void testGetTotalCost2() throws Throwable {
		CartItem testedObject = new CartItem(5, -10, 1, 0.0);
		double result = testedObject.getTotalCost();
		assertEquals(0.0, result, 0.0);
		// No exception thrown
	}

	/**
	 * Utility main method. Runs the test cases defined in this test class.
	 * 
	 * Usage: java CartItemTest
	 * 
	 * @param args
	 *            command line arguments are not needed
	 * @author White_Birds
	 */
	public static void main(String[] args) {
		Result result = JUnitCore.runClasses(CartItemTest.class);
		for (Failure failure : result.getFailures()) {
			System.err.println(failure.toString());
			System.err.println();
		}
		System.out.println(result.wasSuccessful());
	}
}