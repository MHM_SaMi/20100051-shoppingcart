/*
 * Copyright (C) 2014 White_Birds Open Source Project 
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at 
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software 
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and 
 * limitations under the License.
 */

package Exception;

/**
 * Java custom exception class used to be thrown when specific cart item cannot
 * be found in the shopping cart
 * 
 * @author White_Birds
 */

public class CartItemNotFoundException extends Exception {

	/**
	 * Serial version UID used to make sure that the same version code is
	 * running every time without any changing in this class source code
	 * 
	 * @author White_Birds
	 */
	private static final long serialVersionUID = 1079609827685919401L;

	/**
	 * Constructs a new exception with null as its detail message. The cause is
	 * not initialized
	 * 
	 * @author White_Birds
	 */
	public CartItemNotFoundException() {
		super();
	}

	/**
	 * Constructs a new exception with the specified detail message. The cause
	 * is not initialized
	 * 
	 * @param message
	 *            the detail message. The detail message is saved for later
	 *            retrieval by the Throwable.getMessage() method
	 * 
	 * @author White_Birds
	 */
	public CartItemNotFoundException(String message) {
		super(message);
	}

	/**
	 * Constructs a new exception with the specified cause and a detail message
	 * of (cause==null ? null : cause.toString())
	 * 
	 * @param cause
	 *            the cause (which is saved for later retrieval by the
	 *            Throwable.getCause() method). (A null value is permitted, and
	 *            indicates that the cause is nonexistent or unknown.)
	 * 
	 * @author White_Birds
	 */
	public CartItemNotFoundException(Throwable cause) {
		super(cause);
	}

	/**
	 * Constructs a new exception with the specified detail message and cause
	 * 
	 * @param message
	 *            the detail message (which is saved for later retrieval by the
	 *            Throwable.getMessage() method)
	 * @param cause
	 *            the cause (which is saved for later retrieval by the
	 *            Throwable.getCause() method). (A null value is permitted, and
	 *            indicates that the cause is nonexistent or unknown
	 * 
	 * @author White_Birds
	 */
	public CartItemNotFoundException(String message, Throwable cause) {
		super(message, cause);
	}
}