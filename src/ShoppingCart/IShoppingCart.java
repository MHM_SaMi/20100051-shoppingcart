/*
 * Copyright (C) 2014 White_Birds Open Source Project 
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at 
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software 
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and 
 * limitations under the License.
 */

package ShoppingCart;

import java.util.ArrayList;
import java.util.Date;

import Exception.CartItemNotFoundException;

/**
 * IShoppingCart is a java interface which describes the common functionalities
 * among all Shopping Cart types to ensure open close principle
 * 
 * @author White_Birds
 */

public interface IShoppingCart {

	/**
	 * Get current shopping cart id
	 * 
	 * @return shopping cart id
	 * @author White_Birds
	 */
	public int getId();

	/**
	 * Get current shopping cart session id
	 * 
	 * @return shopping cart session id
	 * @author White_Birds
	 */
	public int getSessionID();

	/**
	 * Get current shopping cart customer id
	 * 
	 * @return shopping cart customer id
	 * @author White_Birds
	 */
	public int getCustomerID();

	/**
	 * Add cart item object to the shopping cart
	 * 
	 * @param cart
	 *            item object to be added to the shopping cart
	 * @author White_Birds
	 */
	public void addItem(ICartItem item);

	/**
	 * Update quantity of a cart item object if exists inside the shopping cart
	 * by a new quantity. otherwise, throws a CartItemNotFoundException
	 * 
	 * @param cart
	 *            item id which will be updated
	 * @param new cart item quantity
	 * @throws CartItemNotFoundException
	 * @author White_Birds
	 */
	public void updateQuantity(int cartItemID, int newQuantity)
			throws CartItemNotFoundException;

	/**
	 * Remove cart item object if exists inside the shopping cart otherwise,
	 * throws a CartItemNotFoundException
	 * 
	 * @param cart
	 *            item id which will be removed
	 * @throws CartItemNotFoundException
	 * @author White_Birds
	 */
	public void removeItem(int cartItemId) throws CartItemNotFoundException;

	/**
	 * Get cart item object from the shopping cart if exists. Otherwise, throws
	 * CartItemNotFoundException
	 * 
	 * @param cart
	 *            item product id
	 * @return cart item object
	 * @throws CartItemNotFoundException
	 */
	public ICartItem getItem(int productID) throws CartItemNotFoundException;

	/**
	 * Get shopping cart cart items arrayList which contains all cart items
	 * inside the shopping cart. Otherwise, return empty arrayList of cart items
	 * 
	 * @return shopping cart arrayList of ICartItem type
	 * @author White_Birds
	 */
	public ArrayList<ICartItem> getItems();

	/**
	 * Get current number of cart items inside the shopping cart
	 * 
	 * @return number of cart items
	 * @author White_Birds
	 */
	public int countItems();

	/**
	 * Get last access date for the shopping cart if shopping is just created
	 * right now, it will return the current date
	 * 
	 * @return last access shopping cart date
	 * @author White_Birds
	 */
	public Date getLastAccessedDate();
}