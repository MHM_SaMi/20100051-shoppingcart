/*
 * Copyright (C) 2014 White_Birds Open Source Project 
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at 
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software 
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and 
 * limitations under the License.
 */

package ShoppingCart;

import java.io.Serializable;

import Exception.IlegalQuantityException;

/**
 * Java class describes the attributes and functionalities of the Cart Item
 * 
 * @author White_Birds
 */

public class CartItem implements ICartItem, Serializable {

	/**
	 * Used in serializing and de-serializing process to make sure that the same
	 * version code is running in both serializing and de-serializing process
	 * 
	 * @author White_Birds
	 */
	private static final long serialVersionUID = 253602101316349321L;

	public int id;

	public int productId;

	public int quantity;

	public double price;

	/**
	 * Get current cart item id
	 * 
	 * @return cart item id
	 * @author White_Birds
	 * @see ShoppingCart.ICartItem#getId()
	 */
	public int getId() {
		return this.id;
	}

	/**
	 * Get current cart item product id
	 * 
	 * @return cart item product id
	 * @author White_Birds
	 * @see ShoppingCart.ICartItem#getProductId()
	 */
	public int getProductId() {
		return this.productId;
	}

	/**
	 * Get current cart item quantity
	 * 
	 * @return cart item quantity
	 * @author White_Birds
	 * @see ShoppingCart.ICartItem#getQuantity()
	 */
	public int getQuantity() {
		return this.quantity;
	}

	/**
	 * Get current cart item unit price for the product specified in the cart
	 * item product id
	 * 
	 * @return cart item unit price
	 * @author White_Birds
	 * @see ShoppingCart.ICartItem#getUnitPrice()
	 */
	public double getUnitPrice() {
		return this.price;
	}

	/**
	 * Get current total cost of all product units inside the cart item
	 * 
	 * @return cart item total cost
	 * @author White_Birds
	 * @see ShoppingCart.ICartItem#getTotalCost()
	 */
	public double getTotalCost() {
		return this.quantity * this.price;
	}

	/**
	 * Set cart item id
	 * 
	 * @param cart
	 *            item new id
	 * @author White_Birds
	 * @see ShoppingCart.ICartItem#setId(int)
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 * Set cart item product id
	 * 
	 * @param cart
	 *            item new product id
	 * @author White_Birds
	 * @see ShoppingCart.ICartItem#setProductId(int)
	 */
	public void setProductId(int id) {
		this.productId = id;
	}

	/**
	 * Set cart item quantity
	 * 
	 * @param cart
	 *            item new quantity
	 * @author White_Birds
	 * @see ShoppingCart.ICartItem#setQuantity(int)
	 */
	public void setQuantity(int quantity) {
		if (!(quantity > 0)) {
			throw new IlegalQuantityException(
					"Cart item quantity should be greater than 0"
							+ "\nCannot set the quantity for the cart item "
							+ this.getId());
		} else {
			this.quantity = quantity;
		}
	}

	/**
	 * Set cart item unit price for the product specified in the cart item
	 * product id
	 * 
	 * @param cart
	 *            item new unit price
	 * @author White_Birds
	 * @see ShoppingCart.ICartItem#setUnitPrice(double)
	 */
	public void setUnitPrice(double price) {
		this.price = price;
	}

	/**
	 * Class constructor set default values for Cart item id, product id,
	 * quantity and price
	 * 
	 * @param cart
	 *            item id
	 * @param cart
	 *            item product Id
	 * @param cart
	 *            item quantity
	 * @param cart
	 *            item unit price
	 * @author White_Birds
	 */
	public CartItem(int id, int productId, int quantity, double price) {
		if (!(quantity > 0)) {
			throw new IlegalQuantityException(
					"Cart item quantity should be greater than 0"
							+ "\nCannot create the cart item " + id);
		} else {
			this.id = id;
			this.productId = productId;
			this.quantity = quantity;
			this.price = price;
		}
	}
}