/*
 * Copyright (C) 2014 White_Birds Open Source Project 
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at 
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software 
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and 
 * limitations under the License.
 */

package ShoppingCart;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;

import Exception.CartItemNotFoundException;

/**
 * Java class describes the attributes and functionalities of the Shopping Cart
 * 
 * @author White_Birds
 */

public class ShoppingCart implements IShoppingCart, Serializable {

	/**
	 * Used in serializing and de-serializing process to make sure that the same
	 * version code is running in both serializing and de-serializing process
	 * 
	 * @author White_Birds
	 */
	private static final long serialVersionUID = 1113409890934713569L;

	public int id;

	public int sessionId;

	public int customerId;

	public Date lastAccessed;

	public ArrayList<ICartItem> itemsArray;

	/**
	 * Get current shopping cart id
	 * 
	 * @return shopping cart id
	 * @author White_Birds
	 * @see ShoppingCart.IShoppingCart#getId()
	 */
	public int getId() {
		return this.id;
	}

	/**
	 * Get current shopping cart session id
	 * 
	 * @return shopping cart session id
	 * @author White_Birds
	 * @see ShoppingCart.IShoppingCart#getSessionID()
	 */
	public int getSessionID() {
		return this.sessionId;
	}

	/**
	 * Get current shopping cart customer id
	 * 
	 * @return shopping cart customer id
	 * @author White_Birds
	 * @see ShoppingCart.IShoppingCart#getCustomerID()
	 */
	public int getCustomerID() {
		return this.customerId;
	}

	/**
	 * Add cart item object to the shopping cart
	 * 
	 * @param cart
	 *            item object to be added to the shopping cart
	 * @author White_Birds
	 * @see ShoppingCart.IShoppingCart#addItem(ShoppingCart.ICartItem)
	 */
	public void addItem(ICartItem item) {
		this.lastAccessed = new Date();
		this.itemsArray.add(item);
	}

	/**
	 * Update quantity of a cart item object if exists inside the shopping cart
	 * by a new quantity. otherwise, throws a CartItemNotFoundException
	 * 
	 * @param cart
	 *            item id which will be updated
	 * @param new cart item quantity
	 * @throws CartItemNotFoundException
	 * @author White_Birds
	 * @see ShoppingCart.IShoppingCart#updateQuantity(int, int)
	 */
	public void updateQuantity(int cartItemID, int newQuantity)
			throws CartItemNotFoundException {
		this.lastAccessed = new Date();
		boolean isFound = false;
		for (int i = 0; i < this.itemsArray.size(); i++) {
			if (this.itemsArray.get(i).getId() == cartItemID) {
				this.itemsArray.get(i).setQuantity(newQuantity);
				isFound = true;
				break;
			}
		}

		if (isFound == false) {
			throw new CartItemNotFoundException("Cart item with id "
					+ cartItemID
					+ " cannot be found in the shopping cart with id "
					+ this.getId());
		}
	}

	/**
	 * Remove cart item object if exists inside the shopping cart otherwise,
	 * throws a CartItemNotFoundException
	 * 
	 * @param cart
	 *            item id which will be removed
	 * @throws CartItemNotFoundException
	 * @author White_Birds
	 * @see ShoppingCart.IShoppingCart#removeItem(int)
	 */
	public void removeItem(int cartItemId) throws CartItemNotFoundException {
		this.lastAccessed = new Date();
		boolean isFound = false;
		for (int i = 0; i < this.itemsArray.size(); i++) {
			if (this.itemsArray.get(i).getId() == cartItemId) {
				this.itemsArray.remove(i);
				isFound = true;
				break;
			}
		}

		if (isFound == false) {
			throw new CartItemNotFoundException("Cart item with id "
					+ cartItemId
					+ " cannot be found in the shopping cart with id "
					+ this.getId());
		}
	}

	/**
	 * Get cart item object from the shopping cart if exists. Otherwise, throws
	 * CartItemNotFoundException
	 * 
	 * @param cart
	 *            item product id
	 * @return cart item object
	 * @throws CartItemNotFoundException
	 * @see ShoppingCart.IShoppingCart#getItem(int)
	 */
	public ICartItem getItem(int productID) throws CartItemNotFoundException {
		this.lastAccessed = new Date();
		ICartItem item = null;
		for (int i = 0; i < this.itemsArray.size(); i++) {
			if (this.itemsArray.get(i).getProductId() == productID) {
				item = this.itemsArray.get(i);
				break;
			}
		}

		if (item == null) {
			throw new CartItemNotFoundException("Cart item with product id "
					+ productID
					+ " cannot be found in the shopping cart with id "
					+ this.getId());
		}
		return item;
	}

	/**
	 * Get shopping cart cart items arrayList which contains all cart items
	 * inside the shopping cart. Otherwise, return empty arrayList of cart items
	 * 
	 * @return shopping cart arrayList of ICartItem type
	 * @author White_Birds
	 * @see ShoppingCart.IShoppingCart#getItems()
	 */
	public ArrayList<ICartItem> getItems() {
		return this.itemsArray;
	}

	/**
	 * Get current number of cart items inside the shopping cart
	 * 
	 * @return number of cart items
	 * @author White_Birds
	 * @see ShoppingCart.IShoppingCart#countItems()
	 */
	public int countItems() {
		this.lastAccessed = new Date();
		return this.itemsArray.size();
	}

	/**
	 * Get last access date for the shopping cart if shopping is just created
	 * right now, it will return the current date
	 * 
	 * @return last access shopping cart date
	 * @author White_Birds
	 * @see ShoppingCart.IShoppingCart#getLastAccessedDate()
	 */
	public Date getLastAccessedDate() {
		return this.lastAccessed;
	}

	/**
	 * Class constructor Set default values for Shopping Cart ID, customer ID,
	 * session ID and last accessed date
	 * 
	 * @param shopping
	 *            cart ID
	 * @param customer
	 *            ID
	 * @param session
	 *            ID
	 * @author White_Birds
	 */
	public ShoppingCart(int cartID, int customerID, int sessionID) {
		this.lastAccessed = new Date();
		this.id = cartID;
		this.customerId = customerID;
		this.sessionId = sessionID;
		this.itemsArray = new ArrayList<ICartItem>();
	}
}