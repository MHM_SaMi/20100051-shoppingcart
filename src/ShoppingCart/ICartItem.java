/*
 * Copyright (C) 2014 White_Birds Open Source Project 
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at 
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software 
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and 
 * limitations under the License.
 */

package ShoppingCart;

/**
 * ICartItem is a java interface which describes the common functionalities
 * among all Cart Item types to ensure open close principle
 * 
 * @author White_Birds
 */

public interface ICartItem {

	/**
	 * Get current cart item id
	 * 
	 * @return cart item id
	 * @author White_Birds
	 */
	public int getId();

	/**
	 * Get current cart item product id
	 * 
	 * @return cart item product id
	 * @author White_Birds
	 */
	public int getProductId();

	/**
	 * Get current cart item quantity
	 * 
	 * @return cart item quantity
	 * @author White_Birds
	 */
	public int getQuantity();

	/**
	 * Get current cart item unit price for the product specified in the cart
	 * item product id
	 * 
	 * @return cart item unit price
	 * @author White_Birds
	 */
	public double getUnitPrice();

	/**
	 * Get current total cost of all product units inside the cart item
	 * 
	 * @return cart item total cost
	 * @author White_Birds
	 */
	public double getTotalCost();

	/**
	 * Set cart item id
	 * 
	 * @param cart
	 *            item new id
	 * @author White_Birds
	 */
	public void setId(int id);

	/**
	 * Set cart item product id
	 * 
	 * @param cart
	 *            item new product id
	 * @author White_Birds
	 */
	public void setProductId(int id);

	/**
	 * Set cart item quantity
	 * 
	 * @param cart
	 *            item new quantity
	 * @author White_Birds
	 */
	public void setQuantity(int quantity);

	/**
	 * Set cart item unit price for the product specified in the cart item
	 * product id
	 * 
	 * @param cart
	 *            item new unit price
	 * @author White_Birds
	 */
	public void setUnitPrice(double price);
}