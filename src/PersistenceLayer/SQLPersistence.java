/*
 * Copyright (C) 2014 White_Birds Open Source Project 
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at 
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software 
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and 
 * limitations under the License.
 */

package PersistenceLayer;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import Exception.SQL_Exception;
import ShoppingCart.CartItem;
import ShoppingCart.ICartItem;
import ShoppingCart.IShoppingCart;
import ShoppingCart.ShoppingCart;

/**
 * Java class describes the attributes and functionalities of the SQL
 * Persistence
 * 
 * @author White_Birds
 */

public class SQLPersistence implements IPersistenceMechanism {
	private static IPersistenceMechanism instance;
	private Connection c;

	/**
	 * Save Shopping cart object to the SQLPersistence, it could throw different
	 * types of exceptions
	 * 
	 * @param shopping
	 *            cart object to be saved
	 * @throws SQLException
	 * @author White_Birds
	 * @see PersistenceLayer.IPersistenceMechanism#save(ShoppingCart.IShoppingCart)
	 */
	public void save(IShoppingCart cart) throws java.sql.SQLException {
		Statement stmt1 = null, stmt2 = null, stmt3 = null, stmt4 = null, stmt5 = null;
		ResultSet rs1 = null, rs2 = null;

		stmt1 = c.createStatement();
		String query1 = "SELECT id, quantity FROM CartItem WHERE ShoppingCartID = "
				+ cart.getId() + ";";
		rs1 = stmt1.executeQuery(query1);
		while (rs1.next()) {
			int id = rs1.getInt("id");
			boolean found = false;
			for (int i = 0; i < cart.getItems().size(); i++) {
				if (cart.getItems().get(i).getId() == id) {
					int quantity = rs1.getInt("quantity");
					if (cart.getItems().get(i).getQuantity() != quantity) {
						stmt2 = c.createStatement();
						String query2 = "UPDATE CartItem SET quantity = "
								+ cart.getItems().get(i).getQuantity()
								+ " WHERE id = " + id + ";";
						stmt2.executeUpdate(query2);
					}
					found = true;
					break;
				}
			}
			if (found == false) {
				stmt3 = c.createStatement();
				String query3 = "DELETE FROM CartItem WHERE id = " + id + ";";
				stmt3.executeUpdate(query3);
			}
		}

		for (int i = 0; i < cart.getItems().size(); i++) {
			stmt4 = c.createStatement();
			String query4 = "SELECT id FROM CartItem WHERE id = "
					+ cart.getItems().get(i).getId() + " LIMIT 1;";
			rs2 = stmt4.executeQuery(query4);
			int size = 0;
			for (; rs2.next(); size++)
				;
			if (size == 0) {
				stmt5 = c.createStatement();
				String query5 = "INSERT INTO CartItem (id, productId, quantity, price, ShoppingCartID) "
						+ "VALUES ("
						+ cart.getItems().get(i).getId()
						+ ", "
						+ cart.getItems().get(i).getProductId()
						+ ", "
						+ cart.getItems().get(i).getQuantity()
						+ ", "
						+ cart.getItems().get(i).getUnitPrice()
						+ ", "
						+ cart.getId() + ");";
				stmt5.executeUpdate(query5);
			}
		}

		if (rs1 != null || rs2 != null || stmt1 != null || stmt2 != null
				|| stmt3 != null || stmt4 != null || stmt5 != null) {
			if (rs1 != null) {
				rs1.close();
			}
			if (rs2 != null) {
				rs2.close();
			}
			if (stmt1 != null) {
				stmt1.close();
			}
			if (stmt2 != null) {
				stmt2.close();
			}
			if (stmt3 != null) {
				stmt3.close();
			}
			if (stmt4 != null) {
				stmt4.close();
			}
			if (stmt5 != null) {
				stmt5.close();
			}
		}
	}

	/**
	 * Load a shopping cart object into memory using its id, the method could
	 * throws an exception in case that the shopping cart cannot be found in the
	 * SQLPersistence
	 * 
	 * @param shopping
	 *            cart id to be loaded into memory
	 * @return loaded shopping cart object
	 * @throws SQLException
	 * @throws SQL_Exception
	 * @author White_Birds
	 * @see PersistenceLayer.IPersistenceMechanism#loadCart(int)
	 */
	public IShoppingCart loadCart(int cartID) throws java.sql.SQLException,
			SQL_Exception {
		Statement stmt1 = null, stmt2 = null;
		ResultSet rs1 = null, rs2 = null;
		IShoppingCart cart = null;
		int customerID = 0;
		int sessionID = 0;
		boolean isFound = false;

		stmt1 = c.createStatement();
		String query1 = "SELECT customerId, sessionId FROM ShoppingCart WHERE id = "
				+ cartID + ";";
		rs1 = stmt1.executeQuery(query1);
		while (rs1.next()) {
			customerID = rs1.getInt("customerId");
			sessionID = rs1.getInt("sessionId");
			if (customerID != 0) {
				isFound = true;
				break;
			}
		}

		if (isFound == false) {
			throw new SQL_Exception("Shopping cart with id " + cartID
					+ " cannot be found in the 'SQL' database");
		}

		if (customerID != 0) {
			cart = new ShoppingCart(cartID, customerID, sessionID);

			stmt2 = c.createStatement();
			String query2 = "SELECT * FROM CartItem WHERE ShoppingCartID = "
					+ cartID + ";";
			rs2 = stmt2.executeQuery(query2);
			while (rs2.next()) {
				int id = rs2.getInt("id");
				int productId = rs2.getInt("productId");
				int quantity = rs2.getInt("quantity");
				double price = rs2.getDouble("price");
				ICartItem item = new CartItem(id, productId, quantity, price);
				cart.addItem(item);
			}

			if (rs1 != null || rs1 != null || stmt1 != null || stmt2 != null) {
				if (rs1 != null) {
					rs1.close();
				}
				if (rs2 != null) {
					rs2.close();
				}

				if (stmt1 != null) {
					stmt1.close();
				}
				if (stmt2 != null) {
					stmt2.close();
				}
			}
		}
		return cart;
	}

	/**
	 * Create an instance of the shopping cart using SQLPersistence to store the
	 * shopping cart on the disk, the method could throws exception in case of
	 * some failure happens. The method will generate the shopping cart
	 * automatically using singleton object of its SQLPersistence to guarantee
	 * that the shopping cart is unique for all shopping cart objects created
	 * 
	 * @param shopping
	 *            cart session id to be created
	 * @param shopping
	 *            cart customer id to be created
	 * @return created shopping cart object with default values
	 * @throws SQLException
	 * @author White_Birds
	 * @see PersistenceLayer.IPersistenceMechanism#createCart(int, int)
	 */
	public IShoppingCart createCart(int sessionID, int customerID)
			throws SQLException {
		IShoppingCart cart = null;
		Statement stmt1 = null, stmt2 = null;
		ResultSet rs1 = null;
		int cartID = 1;

		stmt1 = c.createStatement();
		String query1 = "SELECT id FROM ShoppingCart ORDER BY id DESC LIMIT 1;";
		rs1 = stmt1.executeQuery(query1);
		while (rs1.next()) {
			cartID = rs1.getInt("id") + 1;
		}
		stmt2 = c.createStatement();
		String query2 = "INSERT INTO ShoppingCart (id, sessionId, customerId) "
				+ "VALUES (" + cartID + ", " + sessionID + ", " + customerID
				+ ");";
		stmt2.executeUpdate(query2);

		if (rs1 != null || stmt1 != null || stmt2 != null) {
			if (rs1 != null) {
				rs1.close();
			}
			if (stmt1 != null) {
				stmt1.close();
			}
			if (stmt2 != null) {
				stmt2.close();
			}
		}
		cart = new ShoppingCart(cartID, customerID, sessionID);
		return cart;
	}

	/**
	 * Remove a shopping cart along with its all associated data if exists in
	 * the SQLPersistence file. the method could throws exception in case of
	 * some failure happens.
	 * 
	 * @param shopping
	 *            cart id to be removed from the disk
	 * @throws SQLException
	 * @author White_Birds
	 * @throws SQL_Exception
	 * @see PersistenceLayer.IPersistenceMechanism#removeCart(ShoppingCart.IShoppingCart)
	 */
	public void removeCart(IShoppingCart cart) throws SQLException,
			SQL_Exception {
		Statement stmt1 = null, stmt2 = null;

		stmt1 = c.createStatement();
		String query1 = "DELETE FROM CartItem WHERE ShoppingCartID = "
				+ cart.getId() + ";";

		int affectedRowsCount = stmt1.executeUpdate(query1);

		if (affectedRowsCount == 0) {
			throw new SQL_Exception("Shopping cart with id " + cart.getId()
					+ " cannot be found in the 'SQL' database");
		}

		stmt2 = c.createStatement();
		String query2 = "DELETE FROM ShoppingCart WHERE id = " + cart.getId()
				+ ";";
		stmt2.executeUpdate(query2);
		if (stmt1 != null || stmt2 != null) {
			if (stmt1 != null) {
				stmt1.close();
			}
			if (stmt2 != null) {
				stmt2.close();
			}
		}
	}

	/**
	 * Thread safe version of the Singleton as it uses synchronization and lock
	 * only the creation of a new instance. So, only if an instance is not
	 * created yet, it locks this part so only one thread dose sit. The
	 * instantiation of Singleton happens at the time getInstance is called for
	 * the first time. Synchronization prevents creating 2 instances by 2
	 * threads
	 * 
	 * @return SQL Singleton persistence mechanism object
	 * @throws ClassNotFoundException
	 * @throws SQLException
	 * @author White_Birds
	 */
	public static IPersistenceMechanism getInstance()
			throws ClassNotFoundException, SQLException {
		if (instance == null) {
			synchronized (SQLPersistence.class) {
				if (instance == null) {
					instance = new SQLPersistence();
				}
			}
		}
		return instance;
	}

	/**
	 * Load a shopping cart object into memory using its id, the method could
	 * throws an exception in case that the shopping cart cannot be found in the
	 * SQLPersistence
	 * 
	 * @param shopping
	 *            cart session id to be loaded into memory
	 * @param shopping
	 *            cart customer id to be loaded into memory
	 * @return loaded shopping cart object
	 * @throws SQLException
	 * @throws SQL_Exception
	 * @author White_Birds
	 * @see PersistenceLayer.IPersistenceMechanism#loadCart(int, int)
	 */
	public IShoppingCart loadCart(int sessionID, int customerID)
			throws SQLException, SQL_Exception {
		Statement stmt1 = null, stmt2 = null;
		ResultSet rs1 = null, rs2 = null;
		IShoppingCart cart = null;
		int cartID = 0;
		boolean isFound = false;

		stmt1 = c.createStatement();
		String query1 = "SELECT id FROM ShoppingCart WHERE customerId = "
				+ customerID + " AND sessionId = " + sessionID
				+ " ORDER BY customerId ASC, sessionId ASC LIMIT 1;";
		rs1 = stmt1.executeQuery(query1);

		while (rs1.next()) {
			isFound = true;
			cartID = rs1.getInt("id");
			if (cartID != 0) {
				break;
			}
		}

		if (isFound == false) {
			throw new SQL_Exception("Shopping cart with customer id "
					+ customerID + " and session id " + sessionID
					+ " cannot be found in the 'SQL' database");
		}

		if (cartID != 0) {
			cart = new ShoppingCart(cartID, customerID, sessionID);

			stmt2 = c.createStatement();
			String query2 = "SELECT * FROM CartItem WHERE ShoppingCartID = "
					+ cartID + ";";
			rs2 = stmt2.executeQuery(query2);
			while (rs2.next()) {
				int id = rs2.getInt("id");
				int productId = rs2.getInt("productId");
				int quantity = rs2.getInt("quantity");
				double price = rs2.getDouble("price");
				ICartItem item = new CartItem(id, productId, quantity, price);
				cart.addItem(item);
			}

			if (rs1 != null || rs1 != null || stmt1 != null || stmt2 != null) {
				if (rs1 != null) {
					rs1.close();
				}
				if (rs2 != null) {
					rs2.close();
				}

				if (stmt1 != null) {
					stmt1.close();
				}
				if (stmt2 != null) {
					stmt2.close();
				}
			}
		}
		return cart;
	}

	/**
	 * Class private constructor in order to prevent class instantiation to
	 * perform singleton implementation. this constructor build the connection
	 * to the database file once to avoid the multiple connection to the same
	 * database more the one time
	 * 
	 * @throws ClassNotFoundException
	 * @throws SQLException
	 */
	private SQLPersistence() throws ClassNotFoundException, SQLException {
		Statement stmt1 = null, stmt2 = null;

		Class.forName("org.sqlite.JDBC");
		c = DriverManager.getConnection("jdbc:sqlite:sql.db");

		stmt1 = c.createStatement();
		String query1 = "CREATE TABLE IF NOT EXISTS ShoppingCart"
				+ "(id 			  	INT     	PRIMARY KEY, "
				+ "sessionId       	INT    		NOT NULL, "
				+ "customerId     	INT     	NOT NULL);";
		stmt1.executeUpdate(query1);

		stmt2 = c.createStatement();
		String query2 = "CREATE TABLE IF NOT EXISTS CartItem"
				+ "(id 			  	INT     	PRIMARY KEY, "
				+ "productId       	INT    		NOT NULL, "
				+ "quantity     	INT     	NOT NULL, "
				+ "price   			REAL		NOT NULL, "
				+ "ShoppingCartID	INT			NOT NULL, "
				+ "FOREIGN KEY(ShoppingCartID) REFERENCES ShoppingCart(id));";
		stmt2.executeUpdate(query2);

		if (stmt1 != null || stmt2 != null) {
			if (stmt1 != null) {
				stmt1.close();
			}
			if (stmt2 != null) {
				stmt2.close();
			}
		}
	}
}