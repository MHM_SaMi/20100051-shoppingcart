/*
 * Copyright (C) 2014 White_Birds Open Source Project 
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at 
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software 
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and 
 * limitations under the License.
 */

package PersistenceLayer;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.SQLException;

import Exception.FileException;
import Exception.SQL_Exception;
import ShoppingCart.IShoppingCart;

/**
 * IPersistenceMechanism is a java interface which describes the common
 * functionalities among all persistence mechanism types to ensure open close
 * principle
 * 
 * @author White_Birds
 */

public interface IPersistenceMechanism {

	/**
	 * Save Shopping cart object to one of the available persistence mechanisms,
	 * it could throw different types of exceptions depends on the persistence
	 * mechanism type
	 * 
	 * @param shopping
	 *            cart object to be saved
	 * @throws FileNotFoundException
	 * @throws ClassNotFoundException
	 * @throws IOException
	 * @throws SQLException
	 * @author White_Birds
	 */
	public void save(IShoppingCart cart) throws FileNotFoundException,
			ClassNotFoundException, IOException, SQLException;

	/**
	 * Load a shopping cart object into memory using its id, the method could
	 * throws an exception in case that the shopping cart cannot be found in the
	 * used persistence mechanism
	 * 
	 * @param shopping
	 *            cart id to be loaded into memory
	 * @return loaded shopping cart object
	 * @throws FileNotFoundException
	 * @throws ClassNotFoundException
	 * @throws IOException
	 * @throws FileException
	 * @throws SQLException
	 * @throws SQL_Exception
	 * @author White_Birds
	 */
	public IShoppingCart loadCart(int cartID) throws FileNotFoundException,
			ClassNotFoundException, IOException, FileException, SQLException,
			SQL_Exception;

	/**
	 * Create an instance of the shopping cart depends on the persistence
	 * mechanism type that will used to store the shopping cart on the disk, the
	 * method could throws exception in case of some failure happens. The method
	 * will generate the shopping cart automatically using singleton object of
	 * the its persistence mechanism to guarantee that the shopping cart is
	 * unique for all shopping cart objects created.
	 * 
	 * @param shopping
	 *            cart session id to be created
	 * @param shopping
	 *            cart customer id to be created
	 * @return created shopping cart object with default values
	 * @throws FileNotFoundException
	 * @throws ClassNotFoundException
	 * @throws IOException
	 * @throws SQLException
	 * @author White_Birds
	 */
	public IShoppingCart createCart(int sessionID, int customerID)
			throws FileNotFoundException, ClassNotFoundException, IOException,
			SQLException;

	/**
	 * Load a shopping cart object into memory using its id, the method could
	 * throws an exception in case that the shopping cart cannot be found in the
	 * used persistence mechanism
	 * 
	 * @param shopping
	 *            cart session id to be loaded into memory
	 * @param shopping
	 *            cart customer id to be loaded into memory
	 * @return loaded shopping cart object
	 * @throws FileNotFoundException
	 * @throws ClassNotFoundException
	 * @throws IOException
	 * @throws FileException
	 * @throws SQLException
	 * @throws SQL_Exception
	 * @author White_Birds
	 */
	public IShoppingCart loadCart(int sessionID, int customerID)
			throws FileNotFoundException, ClassNotFoundException, IOException,
			FileException, SQLException, SQL_Exception;

	/**
	 * Remove a shopping cart along with its all associated data if exists in
	 * the persistence mechanism file. the method could throws exception in case
	 * of some failure happens.
	 * 
	 * @param shopping
	 *            cart id to be removed from the disk
	 * @throws FileNotFoundException
	 * @throws ClassNotFoundException
	 * @throws IOException
	 * @throws SQLException
	 * @author White_Birds
	 * @throws FileException
	 * @throws SQL_Exception
	 */
	public void removeCart(IShoppingCart cart) throws FileNotFoundException,
			ClassNotFoundException, IOException, SQLException, FileException,
			SQL_Exception;
}