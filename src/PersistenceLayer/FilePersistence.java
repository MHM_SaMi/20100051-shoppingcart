/*
 * Copyright (C) 2014 White_Birds Open Source Project 
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at 
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software 
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and 
 * limitations under the License.
 */

package PersistenceLayer;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;

import Exception.FileException;
import ShoppingCart.IShoppingCart;
import ShoppingCart.ShoppingCart;

/**
 * Java class describes the attributes and functionalities of the File
 * Persistence
 * 
 * @author White_Birds
 */

public class FilePersistence implements IPersistenceMechanism {
	private static IPersistenceMechanism instance;

	/**
	 * Save Shopping cart object to the FilePersistence, it could throw
	 * different types of exceptions
	 * 
	 * @param shopping
	 *            cart object to be saved
	 * @throws FileNotFoundException
	 * @throws ClassNotFoundException
	 * @throws IOException
	 * @author White_Birds
	 * @see PersistenceLayer.IPersistenceMechanism#save(ShoppingCart.IShoppingCart)
	 */
	@SuppressWarnings("unchecked")
	public void save(IShoppingCart cart) throws ClassNotFoundException,
			IOException {
		ArrayList<IShoppingCart> shoppingCartList = new ArrayList<IShoppingCart>();
		BufferedReader br = new BufferedReader(new FileReader("file.ser"));
		if (br.readLine() != null) {
			ObjectInputStream inputStream = new ObjectInputStream(
					new FileInputStream(new File("file.ser")));
			shoppingCartList = (ArrayList<IShoppingCart>) inputStream
					.readObject();
			inputStream.close();
		}
		br.close();

		for (int i = 0; i < shoppingCartList.size(); i++) {
			if (shoppingCartList.get(i).getId() == cart.getId()) {
				shoppingCartList.set(i, cart);
				break;
			}
		}

		ObjectOutputStream outputStream = new ObjectOutputStream(
				new FileOutputStream(new File("file.ser")));
		outputStream.writeObject(shoppingCartList);
		outputStream.close();
	}

	/**
	 * Load a shopping cart object into memory using its id, the method could
	 * throws an exception in case that the shopping cart cannot be found in the
	 * FilePersistence
	 * 
	 * @param shopping
	 *            cart id to be loaded into memory
	 * @return loaded shopping cart object
	 * @throws FileNotFoundException
	 * @throws ClassNotFoundException
	 * @throws IOException
	 * @throws FileException
	 * @author White_Birds
	 * @see PersistenceLayer.IPersistenceMechanism#loadCart(int)
	 */
	@SuppressWarnings("unchecked")
	public IShoppingCart loadCart(int cartID) throws ClassNotFoundException,
			IOException, FileException {
		ArrayList<IShoppingCart> shoppingCartList = new ArrayList<IShoppingCart>();
		IShoppingCart cart = null;
		boolean isFound = false;

		BufferedReader br = new BufferedReader(new FileReader("file.ser"));
		if (br.readLine() != null) {
			ObjectInputStream inputStream = new ObjectInputStream(
					new FileInputStream(new File("file.ser")));
			shoppingCartList = (ArrayList<IShoppingCart>) inputStream
					.readObject();
			inputStream.close();
		}
		br.close();

		for (int i = 0; i < shoppingCartList.size(); i++) {
			if (shoppingCartList.get(i).getId() == cartID) {
				cart = shoppingCartList.get(i);
				isFound = true;
				break;
			}
		}

		if (isFound == false) {
			throw new FileException("Shopping cart with id " + cartID
					+ " cannot be found in the 'file.ser' file");
		}
		return cart;
	}

	/**
	 * Create an instance of the shopping cart using FilePersistence to store
	 * the shopping cart on the disk, the method could throws exception in case
	 * of some failure happens. The method will generate the shopping cart
	 * automatically using singleton object of the its FilePersistence to
	 * guarantee that the shopping cart is unique for all shopping cart objects
	 * created
	 * 
	 * @param shopping
	 *            cart session id to be created
	 * @param shopping
	 *            cart customer id to be created
	 * @return created shopping cart object with default values
	 * @throws FileNotFoundException
	 * @throws ClassNotFoundException
	 * @throws IOException
	 * @author White_Birds
	 * @see PersistenceLayer.IPersistenceMechanism#createCart(int, int)
	 */
	@SuppressWarnings("unchecked")
	public IShoppingCart createCart(int sessionID, int customerID)
			throws ClassNotFoundException, IOException {
		ArrayList<IShoppingCart> shoppingCartList = new ArrayList<IShoppingCart>();
		IShoppingCart cart = null;
		int cartID = 1;

		BufferedReader br = new BufferedReader(new FileReader("file.ser"));
		if (br.readLine() != null) {
			ObjectInputStream inputStream = new ObjectInputStream(
					new FileInputStream(new File("file.ser")));
			shoppingCartList = (ArrayList<IShoppingCart>) inputStream
					.readObject();
			inputStream.close();
		}
		br.close();

		if (shoppingCartList.size() != 0) {
			cartID = shoppingCartList.get(shoppingCartList.size() - 1).getId() + 1;
		}

		cart = new ShoppingCart(cartID, customerID, sessionID);
		shoppingCartList.add(cart);

		ObjectOutputStream outputStream = new ObjectOutputStream(
				new FileOutputStream(new File("file.ser")));
		outputStream.writeObject(shoppingCartList);
		outputStream.close();

		return cart;
	}

	/**
	 * Remove a shopping cart along with its all associated data if exists in
	 * the FilePersistence file. the method could throws exception in case of
	 * some failure happens.
	 * 
	 * @param shopping
	 *            cart id to be removed from the disk
	 * @throws FileNotFoundException
	 * @throws ClassNotFoundException
	 * @throws IOException
	 * @author White_Birds
	 * @throws FileException
	 * @see PersistenceLayer.IPersistenceMechanism#removeCart(ShoppingCart.IShoppingCart)
	 */
	@SuppressWarnings("unchecked")
	public void removeCart(IShoppingCart cart) throws FileNotFoundException,
			ClassNotFoundException, IOException, FileException {
		ArrayList<IShoppingCart> shoppingCartList = new ArrayList<IShoppingCart>();
		BufferedReader br = new BufferedReader(new FileReader("file.ser"));
		boolean isFound = false;

		if (br.readLine() != null) {
			ObjectInputStream inputStream = new ObjectInputStream(
					new FileInputStream(new File("file.ser")));
			shoppingCartList = (ArrayList<IShoppingCart>) inputStream
					.readObject();
			inputStream.close();
		}
		br.close();

		for (int i = 0; i < shoppingCartList.size(); i++) {
			if (shoppingCartList.get(i).getId() == cart.getId()) {
				shoppingCartList.remove(i);
				isFound = true;
				break;
			}
		}

		if (isFound == false) {
			throw new FileException("Shopping cart with id " + cart.getId()
					+ " cannot be found in the 'file.ser' file");
		}

		ObjectOutputStream outputStream = new ObjectOutputStream(
				new FileOutputStream(new File("file.ser")));
		outputStream.writeObject(shoppingCartList);
		outputStream.close();
	}

	/**
	 * Thread safe version of the Singleton as it uses synchronization and lock
	 * only the creation of a new instance. So, only if an instance is not
	 * created yet, it locks this part so only one thread dose sit. The
	 * instantiation of Singleton happens at the time getInstance is called for
	 * the first time. Synchronization prevents creating 2 instances by 2
	 * threads
	 * 
	 * @return File Singleton persistence mechanism object
	 * @author White_Birds
	 * @throws IOException
	 * @throws FileNotFoundException
	 */
	public static IPersistenceMechanism getInstance()
			throws FileNotFoundException, IOException {
		if (instance == null) {
			synchronized (FilePersistence.class) {
				if (instance == null) {
					instance = new FilePersistence();
				}
			}
		}
		return instance;
	}

	/**
	 * Load a shopping cart object into memory using its id, the method could
	 * throws an exception in case that the shopping cart cannot be found in the
	 * FilePersistence
	 * 
	 * @param shopping
	 *            cart session id to be loaded into memory
	 * @param shopping
	 *            cart customer id to be loaded into memory
	 * @return loaded shopping cart object
	 * @throws FileNotFoundException
	 * @throws ClassNotFoundException
	 * @throws IOException
	 * @throws FileException
	 * @author White_Birds
	 * @see PersistenceLayer.IPersistenceMechanism#loadCart(int, int)
	 */
	@SuppressWarnings("unchecked")
	public IShoppingCart loadCart(int sessionID, int customerID)
			throws ClassNotFoundException, IOException, FileException {
		ArrayList<IShoppingCart> shoppingCartList = new ArrayList<IShoppingCart>();
		IShoppingCart cart = null;
		boolean isFound = false;

		BufferedReader br = new BufferedReader(new FileReader("file.ser"));
		if (br.readLine() != null) {
			ObjectInputStream inputStream = new ObjectInputStream(
					new FileInputStream(new File("file.ser")));
			shoppingCartList = (ArrayList<IShoppingCart>) inputStream
					.readObject();
			inputStream.close();
		}
		br.close();

		for (int i = 0; i < shoppingCartList.size(); i++) {
			if (shoppingCartList.get(i).getCustomerID() == customerID
					&& shoppingCartList.get(i).getSessionID() == sessionID) {
				cart = shoppingCartList.get(i);
				isFound = true;
				break;
			}
		}

		if (isFound == false) {
			throw new FileException("Shopping cart with customer id "
					+ customerID + " and session id " + sessionID
					+ " cannot be found in the 'file.ser' file");
		}
		return cart;
	}

	/**
	 * Class private constructor in order to prevent class instantiation to
	 * perform singleton implementation. this constructor creates our 'file.ser'
	 * in case it doesn't exist. Otherwise it well keep the exsit version of the
	 * 'file.ser' file
	 * 
	 * @throws IOException
	 * @throws FileNotFoundException
	 */
	private FilePersistence() throws FileNotFoundException, IOException {
		new FileOutputStream("file.ser", true).close();
	}
}