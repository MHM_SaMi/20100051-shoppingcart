/*
 * Copyright (C) 2014 White_Birds Open Source Project 
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at 
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software 
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and 
 * limitations under the License.
 */

package PersistenceLayer;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.SQLException;

/**
 * Java class which used to Load the mechanism which will be used to save, load
 * and update the shopping cart
 * 
 * @author White_Birds
 */

public class PersistenceFactory {
	/**
	 * Load the mechanism which will be used to save, load and update the
	 * shopping cart, it uses the factory implementation way to create and
	 * return instances of the persistence mechanisms
	 * 
	 * @param savingvmechanism
	 *            type for the shopping cart
	 * @return created class instance depends on saving mechanism type parameter
	 * @throws ClassNotFoundException
	 * @throws SQLException
	 * @author White_Birds
	 * @throws IOException
	 * @throws FileNotFoundException
	 */
	public IPersistenceMechanism loadMechanism(String type)
			throws ClassNotFoundException, SQLException, FileNotFoundException,
			IOException {
		if (type.equals("FilePersistence")) {
			return FilePersistence.getInstance();
		} else if (type.equals("SQLPersistence")) {
			return SQLPersistence.getInstance();
		} else {
			return null;
		}
	}
}