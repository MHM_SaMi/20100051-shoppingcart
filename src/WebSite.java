/*
 * Copyright (C) 2014 White_Birds Open Source Project 
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at 
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software 
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and 
 * limitations under the License.
 */

import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.SQLException;

import Exception.CartItemNotFoundException;
import Exception.FileException;
import Exception.SQL_Exception;
import PersistenceLayer.IPersistenceMechanism;
import PersistenceLayer.PersistenceFactory;
import ShoppingCart.CartItem;
import ShoppingCart.ICartItem;
import ShoppingCart.IShoppingCart;
import ShoppingCart.ShoppingCart;

public class WebSite {

	/**
	 * Utility main method. Runs the test cases defined in this test class.
	 * 
	 * Usage: java FilePersistenceTest
	 * 
	 * @param args
	 *            command line arguments are not needed
	 * @author White_Birds
	 * @throws SQLException
	 * @throws ClassNotFoundException
	 * @throws IOException
	 * @throws FileNotFoundException
	 * @throws SQL_Exception
	 * @throws FileException
	 * @throws CartItemNotFoundException
	 */
	public static void main(String[] args) throws ClassNotFoundException,
			SQLException, FileNotFoundException, IOException, FileException,
			SQL_Exception, CartItemNotFoundException {

		// ----------------------------------------------------------------
		// Integration Testing FilePersistance
		// ----------------------------------------------------------------
		//
		// PersistenceFactory persistenceFactory = new PersistenceFactory();
		// IPersistenceMechanism FilePersistence = persistenceFactory
		// .loadMechanism("FilePersistence");
		//
		// -----------------Create Cart Items-----------------//
		// ICartItem item1 = new CartItem(1, 1, 5, 12.5);
		// ICartItem item2 = new CartItem(2, 2, 10, 15.5);
		// ICartItem item3 = new CartItem(3, 3, 15, 18.5);
		// ICartItem item4 = new CartItem(4, 4, 20, 21.5);
		// ICartItem item5 = new CartItem(5, 5, 25, 24.5);
		// ICartItem item6 = new CartItem(6, 6, 30, 27.5);
		// ICartItem item7 = new CartItem(7, 7, 35, 31.5);
		// ICartItem item8 = new CartItem(8, 8, 40, 34.5);
		// ICartItem badItem = new CartItem(9, 9, -1, 37.5);
		//
		// -----------------Create Shopping Carts-----------------//
		// IShoppingCart cart1 = FilePersistence.createCart(1, 1);
		// cart1.addItem(item1);
		// cart1.addItem(item2);
		// FilePersistence.save(cart1);
		//
		// IShoppingCart cart2 = FilePersistence.createCart(2, 2);
		// cart2.addItem(item3);
		// cart2.addItem(item4);
		// FilePersistence.save(cart2);
		//
		// IShoppingCart cart3 = FilePersistence.createCart(3, 3);
		// cart3.addItem(item5);
		// cart3.addItem(item6);
		// FilePersistence.save(cart3);
		//
		// -----------------Remove Shopping Carts-----------------//
		// IShoppingCart removedCart = new ShoppingCart(1, 0, 0);
		// FilePersistence.removeCart(removedCart);
		//
		// IShoppingCart removedCart = new ShoppingCart(2, 0, 0);
		// FilePersistence.removeCart(removedCart);
		//
		// IShoppingCart removedCart = new ShoppingCart(3, 0, 0);
		// FilePersistence.removeCart(removedCart);
		//
		// -----------------Load Shopping Cart-----------------//
		// IShoppingCart cart = FilePersistence.loadCart(1);
		//
		// -----------------Remove Cart Item-----------------//
		// cart.removeItem(1);
		// cart.removeItem(2);
		// cart.removeItem(3);
		// cart.removeItem(4);
		// cart.removeItem(5);
		// cart.removeItem(6);
		// cart.removeItem(7);
		// cart.removeItem(8);
		// cart.removeItem(9);
		//
		// -----------------Update Quantity of Cart Item-----------------//
		// cart.updateQuantity(1, 50);
		// cart.updateQuantity(2, 100);
		// cart.updateQuantity(3, 150);
		// cart.updateQuantity(4, 200);
		// cart.updateQuantity(5, 250);
		// cart.updateQuantity(6, 300);
		// cart.updateQuantity(7, 350);
		// cart.updateQuantity(8, 400);
		// cart.updateQuantity(9, 450);
		//
		// -----------------Save Shopping Cart-----------------//
		// FilePersistence.save(cart);
		//
		// =============================================================================================
		//
		// -----------------Print Shopping Carts-----------------//
		// System.out.println("cartID: " + cart.getId());
		// System.out.println("customerID: " + cart.getCustomerID());
		// System.out.println("sessionID: " + cart.getSessionID());
		// System.out.println("lastAccessedDate: " +
		// cart.getLastAccessedDate());
		// for (int i = 0; i < cart.getItems().size(); i++) {
		// System.out.println();
		// System.out.println("itemID: " + cart.getItems().get(i).getId());
		// System.out.println("productID: "
		// + cart.getItems().get(i).getProductId());
		// System.out.println("unitPrice: "
		// + cart.getItems().get(i).getUnitPrice());
		// System.out.println("quantity: "
		// + cart.getItems().get(i).getQuantity());
		// System.out.println("totalCost: "
		// + cart.getItems().get(i).getTotalCost());
		// }
		//
		// _________________________________________________________________
		// |-|-|-|-|-|-|-|-|-|-|-|-|-|-|-|-|-|-|-|-|-|-|-|-|-|-|-|-|-|-|-|-|
		// |-|-|-|-|-|-|-|-|-|-|-|-|-|-|-|-|-|-|-|-|-|-|-|-|-|-|-|-|-|-|-|-|
		// |-|-|-|-|-|-|-|-|-|-|-|-|-|-|-|-|-|-|-|-|-|-|-|-|-|-|-|-|-|-|-|-|
		// _________________________________________________________________
		//
		//
		// ----------------------------------------------------------------
		// Integration Testing SQLPersistance
		// ----------------------------------------------------------------
		//
		// PersistenceFactory persistenceFactory = new PersistenceFactory();
		// IPersistenceMechanism SQLPersistence = persistenceFactory
		// .loadMechanism("SQLPersistence");
		//
		// -----------------Create Cart Items-----------------//
		// ICartItem item1 = new CartItem(1, 1, 5, 12.5);
		// ICartItem item2 = new CartItem(2, 2, 10, 15.5);
		// ICartItem item3 = new CartItem(3, 3, 15, 18.5);
		// ICartItem item4 = new CartItem(4, 4, 20, 21.5);
		// ICartItem item5 = new CartItem(5, 5, 25, 24.5);
		// ICartItem item6 = new CartItem(6, 6, 30, 27.5);
		// ICartItem item7 = new CartItem(7, 7, 35, 31.5);
		// ICartItem item8 = new CartItem(8, 8, 40, 34.5);
		// ICartItem badItem = new CartItem(9, 9, -1, 37.5);
		//
		// -----------------Create Shopping Carts-----------------//
		// IShoppingCart cart1 = SQLPersistence.createCart(1, 1);
		// cart1.addItem(item1);
		// cart1.addItem(item2);
		// SQLPersistence.save(cart1);
		//
		// IShoppingCart cart2 = SQLPersistence.createCart(2, 2);
		// cart2.addItem(item3);
		// cart2.addItem(item4);
		// SQLPersistence.save(cart2);
		//
		// IShoppingCart cart3 = SQLPersistence.createCart(3, 3);
		// cart3.addItem(item5);
		// cart3.addItem(item6);
		// SQLPersistence.save(cart3);
		//
		// -----------------Remove Shopping Carts-----------------//
		// IShoppingCart removedCart = new ShoppingCart(1, 0, 0);
		// SQLPersistence.removeCart(removedCart);
		//
		// IShoppingCart removedCart = new ShoppingCart(2, 0, 0);
		// SQLPersistence.removeCart(removedCart);
		//
		// IShoppingCart removedCart = new ShoppingCart(3, 0, 0);
		// SQLPersistence.removeCart(removedCart);
		//
		// -----------------Load Shopping Cart-----------------//
		// IShoppingCart cart = SQLPersistence.loadCart(1);
		//
		// -----------------Remove Cart Item-----------------//
		// cart.removeItem(1);
		// cart.removeItem(2);
		// cart.removeItem(3);
		// cart.removeItem(4);
		// cart.removeItem(5);
		// cart.removeItem(6);
		// cart.removeItem(7);
		// cart.removeItem(8);
		// cart.removeItem(9);
		//
		// -----------------Update Quantity of Cart Item-----------------//
		// cart.updateQuantity(1, 50);
		// cart.updateQuantity(2, 100);
		// cart.updateQuantity(3, 150);
		// cart.updateQuantity(4, 200);
		// cart.updateQuantity(5, 250);
		// cart.updateQuantity(6, 300);
		// cart.updateQuantity(7, 350);
		// cart.updateQuantity(8, 400);
		// cart.updateQuantity(9, 450);
		//
		// -----------------Save Shopping Cart-----------------//
		// SQLPersistence.save(cart);
		//
		// =============================================================================================
		//
		// -----------------Print Shopping Carts-----------------//
		// System.out.println("cartID: " + cart.getId());
		// System.out.println("customerID: " + cart.getCustomerID());
		// System.out.println("sessionID: " + cart.getSessionID());
		// System.out.println("lastAccessedDate: " +
		// cart.getLastAccessedDate());
		// for (int i = 0; i < cart.getItems().size(); i++) {
		// System.out.println();
		// System.out.println("itemID: " + cart.getItems().get(i).getId());
		// System.out.println("productID: "
		// + cart.getItems().get(i).getProductId());
		// System.out.println("unitPrice: "
		// + cart.getItems().get(i).getUnitPrice());
		// System.out.println("quantity: "
		// + cart.getItems().get(i).getQuantity());
		// System.out.println("totalCost: "
		// + cart.getItems().get(i).getTotalCost());
		// }
	}
}